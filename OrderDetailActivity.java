package com.app.bevvi.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.bevvi.R;
import com.app.bevvi.adapter.OrderAdapter;
import com.app.bevvi.api.ApiModule;
import com.app.bevvi.dialog.ProductRatingDialog;
import com.app.bevvi.model.responseModel.LoginResponse;
import com.app.bevvi.model.responseModel.PendingOrderList;
import com.app.bevvi.utils.Const;
import com.app.bevvi.utils.PreferenceManager;
import com.app.bevvi.utils.Utils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.stripe.android.model.Card;

import org.greenrobot.eventbus.EventBus;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * The type Order detail activity.
 */
public class OrderDetailActivity extends BaseActivity implements OnMapReadyCallback {


    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_pickOrderTitle)
    TextView tvPickOrderTitle;

    @BindView(R.id.tv_orderNumber)
    TextView tvOrderNumber;

    @BindView(R.id.tv_ShopName)
    TextView tvShopName;

    @BindView(R.id.tv_Address)
    TextView tvAddress;

    @BindView(R.id.tv_Number)
    TextView tvNumber;

    @BindView(R.id.tv_dateTime)
    TextView tvDateTime;

    @BindView(R.id.tvTaxRate)
    TextView tvTaxRate;

    @BindView(R.id.tvRewards)
    TextView tvRewards;

    @BindView(R.id.tvTotalAmount)
    TextView tvTotalAmount;

    @BindView(R.id.tvEarnPoints)
    TextView tvEarnPoints;

    @BindView(R.id.txtCardType)
    TextView txtCardType;

    @BindView(R.id.txtCardNumber)
    TextView txtCardNumber;

    @BindView(R.id.rv_OrderItem)
    RecyclerView rvOrderItem;

    @BindView(R.id.btn_RateProduct)
    AppCompatButton btnRateProduct;

    @BindView(R.id.rvRewards)
    RelativeLayout rvRewards;

    @BindView(R.id.rewardLine)
    View rewardLine;


    /**
     * The Is past order.
     */
    boolean isPastOrder;
    /**
     * The Is from cart.
     */
    boolean isFromCart;
    /**
     * The Is from notification.
     */
    boolean isFromNotification;
    /**
     * The Pending order list.
     */
    PendingOrderList pendingOrderList;

    /**
     * The Liquor adapter.
     */
    OrderAdapter liquorAdapter;
    /**
     * The List models.
     */
    ArrayList<PendingOrderList.Orderdetail> listModels = new ArrayList<>();
    /**
     * The Alert dialog.
     */
    AlertDialog alertDialog;
    private int TaxPer = 5;

    /**
     * The Google map.
     */
    GoogleMap googleMap;
    /**
     * The Template resource map.
     */
    static final Map<String, Integer> TEMPLATE_RESOURCE_MAP = new HashMap<>();
    /**
     * The Preference manager.
     */
    PreferenceManager preferenceManager;

    /**
     * The Order type.
     */
    String orderType;

    static {
        TEMPLATE_RESOURCE_MAP.put(Card.AMERICAN_EXPRESS, com.stripe.android.R.drawable.ic_amex);
        TEMPLATE_RESOURCE_MAP.put(Card.DINERS_CLUB, com.stripe.android.R.drawable.ic_diners);
        TEMPLATE_RESOURCE_MAP.put(Card.DISCOVER, com.stripe.android.R.drawable.ic_discover);
        TEMPLATE_RESOURCE_MAP.put(Card.JCB, com.stripe.android.R.drawable.ic_jcb);
        TEMPLATE_RESOURCE_MAP.put(Card.MASTERCARD, com.stripe.android.R.drawable.ic_mastercard);
        TEMPLATE_RESOURCE_MAP.put(Card.VISA, com.stripe.android.R.drawable.ic_visa);
        TEMPLATE_RESOURCE_MAP.put(Card.UNKNOWN, com.stripe.android.R.drawable.ic_unknown);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);
        ButterKnife.bind(this);
        preferenceManager = new PreferenceManager(mContext);
        setToolbar();

//        rvRewards.setVisibility(View.GONE);
//        rewardLine.setVisibility(View.GONE);

        MapsInitializer.initialize(this);
        SupportMapFragment mMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mMapFragment.getMapAsync(this);

        if (getIntent().hasExtra("isPastOrder")) {
            isPastOrder = getIntent().getBooleanExtra("isPastOrder", false);
            if (!isPastOrder) {
                btnRateProduct.setVisibility(View.GONE);
                tvPickOrderTitle.setVisibility(View.VISIBLE);
            }
        }
        if (getIntent().hasExtra("isFromCart")) {
            isFromCart = getIntent().getBooleanExtra("isFromCart", false);
            tvPickOrderTitle.setText("Thank you, your order is Pending Acceptance by the Store.");
        }
        if (getIntent().hasExtra("pendingList")) {
            pendingOrderList = (PendingOrderList) getIntent().getSerializableExtra("pendingList");

            tvOrderNumber.setText("Order number #" + pendingOrderList.getOrderNumber());
            tvShopName.setText(pendingOrderList.getEstablishment().getName());
            tvAddress.setText(pendingOrderList.getEstablishment().getAddress());
//            2018-01-10T00:00:00.000Z

            String strDateTime = Utils.formateDateFromstring("yyyy-MM-dd'T'HH:mm:ss.SSS",
                    "yyyy-MM-dd HH:mm", pendingOrderList.getPickupTime());

            String convertedDateFromUtc = Utils.getLocalfromUtc(strDateTime, "yyyy-MM-dd HH:mm", "EEEE MMM dd, yyyy");
            String convertedTimeFromUtc = Utils.getLocalfromUtc(strDateTime, "yyyy-MM-dd HH:mm", "hh:mm a");

            tvDateTime.setText(convertedDateFromUtc + "\n" + convertedTimeFromUtc);

            listModels.clear();
            listModels.addAll(pendingOrderList.getOrderdetails());

            int count = 0;
            for (int i = 0; i < pendingOrderList.getOrderdetails().size(); i++) {
                count = count + pendingOrderList.getOrderdetails().get(i).getQuantity();
            }
            if (isFromCart)
                tvEarnPoints.setText(count * 10 + " pts");
//            calculateTaxAndTotal(listModels);

            if (pendingOrderList.getDiscountApplied() != null && pendingOrderList.getDiscountApplied().getDiscountAmount() != null)
                tvRewards.setText("- $" + String.format("%.2f", Double.valueOf(pendingOrderList.getDiscountApplied().getDiscountAmount())));
            else
                tvRewards.setText("- $0.00");
            if (pendingOrderList.getTotalAmount() != null)
                tvTotalAmount.setText("$" + String.format("%.2f", Double.valueOf(pendingOrderList.getTotalAmount())));
            if (pendingOrderList.getTax() != null)
                tvTaxRate.setText("$" + String.format("%.2f", Double.valueOf(pendingOrderList.getTax())));

            if (!pendingOrderList.getTransactions().isEmpty())
                txtCardNumber.setText(pendingOrderList.getTransactions().get(0).getAdditionalData().getSource().getCard().getLast4());


            setCardDetail(pendingOrderList.getTransactions().get(0).getAdditionalData().getSource().getCard().getBrand());

            if (getIntent().hasExtra("type")) {
                if (getIntent().getExtras() != null && getIntent().getExtras().getString("type") != null) {
                    orderType = getIntent().getExtras().getString("type");
                    if (orderType != null) {
                        setData(orderType,count);
                    }
                }
            }
        }

        if (getIntent().hasExtra("isFromNotification")) {
            if (getIntent().getBooleanExtra("isFromNotification", false)) {
                isFromNotification = getIntent().getBooleanExtra("isFromNotification", false);
                if (getIntent().hasExtra("type")) {
                    if (getIntent().getExtras() != null && getIntent().getExtras().getString("type") != null) {
                        btnRateProduct.setVisibility(View.GONE);
                        orderType = getIntent().getExtras().getString("type");
                        tvPickOrderTitle.setVisibility(View.VISIBLE);
                        if (orderType.equalsIgnoreCase(Const.ORDER_FILLED)) {
                            tvPickOrderTitle.setText("Thank you. Your oder is ready to be picked up");
                        } else if (orderType.equalsIgnoreCase(Const.ORDER_REJECTED)) {
                            tvPickOrderTitle.setText("Sorry your Oder was rejected by the store. Please choose another item.");
                        } else if (orderType.equalsIgnoreCase(Const.ORDER_CANCELLED)) {
//                            tvPickOrderTitle.setText("Your Order Cancelled by the Store because age verification could not be completed successfully.");
                            tvPickOrderTitle.setText("Your Order was Cancelled and a full Credit was Issued.");
                        } else if (orderType.equalsIgnoreCase(Const.ORDER_CANCELLED_AGE)) {

                            tvPickOrderTitle.setText("Your Order Cancelled by the Store because age verification could not be completed successfully.");

                        }
                        getOrderDetailForNotification(getIntent().getStringExtra("orderId"));
                    }
                }
            }
        }

        liquorAdapter = new OrderAdapter(this, listModels);
        rvOrderItem.setHasFixedSize(false);
        rvOrderItem.setNestedScrollingEnabled(false);
        LinearLayoutManager rvInvitedLayoutManagaer = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvOrderItem.setAdapter(liquorAdapter);
        rvOrderItem.setLayoutManager(rvInvitedLayoutManagaer);

        btnRateProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog = new ProductRatingDialog(mContext, listModels, null, new ProductRatingDialog.DialogClickListner() {

                    @Override
                    public void onClickDismissButton() {
                        alertDialog.dismiss();
                    }

                    @Override
                    public void onClickSubmitButton() {
                        alertDialog.dismiss();
                    }
                });
                alertDialog.show();
            }
        });
    }

    private void setData(String orderType, int count) {
        tvPickOrderTitle.setVisibility(View.VISIBLE);
        if (orderType.equalsIgnoreCase(Const.ORDER_TYPE_PENDINGORDER)) {
            tvPickOrderTitle.setText("Thank you, your order is Pending Acceptance by the Store.");
        } else if (orderType.equalsIgnoreCase(Const.ORDER_TYPE_PENDINGPICKUPORDER)) {
            tvPickOrderTitle.setText("Thank you. Your oder is ready to be picked up.");
            if (!pendingOrderList.isViewed())
                setOrderViewed(pendingOrderList.getId());
        } else if (orderType.equalsIgnoreCase(Const.ORDER_TYPE_REJECTEDORDER)) {
            tvPickOrderTitle.setText("Sorry your Oder was rejected by the store. Please choose another item.");
        } else if (orderType.equalsIgnoreCase(Const.ORDER_TYPE_CANCELLEDORDER)) {
//                        tvPickOrderTitle.setText("Your Order Cancelled by the Store because age verification could not be completed successfully.");
            tvPickOrderTitle.setText("Your Order was Cancelled and a full Credit was Issued.");
        } else if (orderType.equalsIgnoreCase(Const.ORDER_TYPE_PASTORDER)) {
            tvPickOrderTitle.setText("Thanks for shopping with us.");
        } else if (orderType.equalsIgnoreCase(Const.ORDER_TYPE_CANCELLEDAGEORDER)) {
            tvPickOrderTitle.setText("Your Order Cancelled by the Store because age verification could not be completed successfully.");
        }
        System.out.println("OrderDetailActivity.onCreate orderType: " + orderType);
        if (orderType.equalsIgnoreCase(Const.ORDER_TYPE_PENDINGORDER) || orderType.equalsIgnoreCase(Const.ORDER_TYPE_PENDINGPICKUPORDER) || this.orderType.equalsIgnoreCase(Const.ORDER_TYPE_PASTORDER)) {
            tvEarnPoints.setText(count * 10 + " pts");
        } else
            tvEarnPoints.setText("0 pts");
    }

    private void setOrderViewed(String id) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("viewed", true);

        Call<ResponseBody> call = ApiModule.apiService().setProductViewed(id, jsonObject);
        call.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response.isSuccessful())
                    System.out.println("setOrderViewed.onResponse : Done -->");

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                System.out.println("setOrderViewed.onFailure : " + t.getMessage());
                Utils.progressDialogDismiss();

            }
        });
    }

    private void setCardDetail(String brand) {
        Drawable img = null;
        System.out.println("OrderDetailActivity.setCartDetail : " + brand);
        if (brand.equalsIgnoreCase(Card.AMERICAN_EXPRESS)) {
            img = getResources().getDrawable(com.stripe.android.R.drawable.ic_amex);
        } else if (brand.equalsIgnoreCase(Card.DINERS_CLUB)) {
            img = getResources().getDrawable(com.stripe.android.R.drawable.ic_diners);
        } else if (brand.equalsIgnoreCase(Card.DISCOVER)) {
            img = getResources().getDrawable(com.stripe.android.R.drawable.ic_discover);
        } else if (brand.equalsIgnoreCase(Card.JCB)) {
            img = getResources().getDrawable(com.stripe.android.R.drawable.ic_jcb);
        } else if (brand.equalsIgnoreCase(Card.MASTERCARD)) {
            img = getResources().getDrawable(com.stripe.android.R.drawable.ic_mastercard);
        } else if (brand.equalsIgnoreCase(Card.VISA)) {
            img = getResources().getDrawable(com.stripe.android.R.drawable.ic_visa);
        } else if (brand.equalsIgnoreCase(Card.UNKNOWN)) {
            img = getResources().getDrawable(com.stripe.android.R.drawable.ic_unknown);
        }
        txtCardType.setText(brand);
        txtCardNumber.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
    }

    private void setToolbar() {

        toolbarTitle.setText("Order Summary");
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!isFromNotification)
                    onBackPressed();
                else {
                    Intent intent = new Intent(mContext, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    finish();
                    startActivity(intent);
                    finishAffinity();
                }

            }
        });


    }

    @Override
    public void onBackPressed() {

        if (!isFromCart) {
            if (Const.isProductRatting)
                EventBus.getDefault().post("doProfilePageRefresh");
            super.onBackPressed();
        } else {
            Intent intent = new Intent(mContext, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            finish();
            startActivity(intent);

        }
    }

    private void getOrderDetailForNotification(final String orderId) {
//        {"where":{"accountId":"123412341234","status":0},"include":["establishment","transactions",{"orderdetails":["product"]}]}
        System.out.println("ProfileFragment.getOrderDetailForNotification orderId : " + orderId);
        LoginResponse loginResponse = preferenceManager.getLoginData();

        JsonObject object = new JsonObject();

        JsonObject where = new JsonObject();
        where.addProperty("id", orderId);


        final JsonArray include = new JsonArray();
        include.add("establishment");
        include.add("transactions");

        JsonArray orderdetails = new JsonArray();
        orderdetails.add("product");


        JsonObject order = new JsonObject();
        order.add("orderdetails", orderdetails);

        include.add(order);

        object.add("where", where);
        object.add("include", include);


        System.out.println("ProfileFragment.getOrderDetailForNotification : " + object.toString());
        Utils.progressDialog(mContext);

        Call<ArrayList<PendingOrderList>> call = ApiModule.apiService().getPastPendingOrder(object.toString(), loginResponse.getAccessToken());
        call.enqueue(new Callback<ArrayList<PendingOrderList>>() {

            @Override
            public void onResponse(Call<ArrayList<PendingOrderList>> call, Response<ArrayList<PendingOrderList>> response) {


                if (response.isSuccessful()) {
                    Utils.progressDialogDismiss();
                    if (!response.body().isEmpty()) {
                        pendingOrderList = response.body().get(0);
                        tvOrderNumber.setText("Order number #" + pendingOrderList.getOrderNumber());
                        tvShopName.setText(pendingOrderList.getEstablishment().getName());
                        tvAddress.setText(pendingOrderList.getEstablishment().getAddress());
//            2018-01-10T00:00:00.000Z

                        String strDateTime = Utils.formateDateFromstring("yyyy-MM-dd'T'HH:mm:ss.SSS",
                                "yyyy-MM-dd HH:mm", pendingOrderList.getPickupTime());

                        String convertedDateFromUtc = Utils.getLocalfromUtc(strDateTime, "yyyy-MM-dd HH:mm", "EEEE MMM dd, yyyy");
                        String convertedTimeFromUtc = Utils.getLocalfromUtc(strDateTime, "yyyy-MM-dd HH:mm", "hh:mm a");

                        tvDateTime.setText(convertedDateFromUtc + "\n" + convertedTimeFromUtc);

                        listModels.clear();
                        listModels.addAll(pendingOrderList.getOrderdetails());
                        liquorAdapter.notifyDataSetChanged();

                        int count = 0;
                        for (int i = 0; i < pendingOrderList.getOrderdetails().size(); i++) {
                            count = count + pendingOrderList.getOrderdetails().get(i).getQuantity();
                        }

                        if (orderType.equalsIgnoreCase(Const.ORDER_FILLED))
                            tvEarnPoints.setText(count * 10 + " pts");
                        else
                            tvEarnPoints.setText("0 pts");
//            calculateTaxAndTotal(listModels);

                        if (pendingOrderList.getDiscountApplied() != null && pendingOrderList.getDiscountApplied().getDiscountAmount() != null)
                            tvRewards.setText("- $" + String.format("%.2f", Double.valueOf(pendingOrderList.getDiscountApplied().getDiscountAmount())));
                        else
                            tvRewards.setText("- $0.00");
                        tvTotalAmount.setText("$" + String.format("%.2f", Double.valueOf(pendingOrderList.getTotalAmount())));
                        tvTaxRate.setText("$" + String.format("%.2f", Double.valueOf(pendingOrderList.getTax())));
                        txtCardNumber.setText(pendingOrderList.getTransactions().get(0).getAdditionalData().getSource().getCard().getLast4());


                        setCardDetail(pendingOrderList.getTransactions().get(0).getAdditionalData().getSource().getCard().getBrand());
                    }

                } else if (response.raw().code() == 401) {
                    Utils.progressDialogDismiss();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (!isFinishing())
                                new AlertDialog.Builder(mContext).setTitle("Alert").setMessage(R.string.session_expire).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        PreferenceManager preferenceManager = new PreferenceManager(mContext);
                                        preferenceManager.clearPrefrences();
                                        preferenceManager.setStringPreference(PreferenceManager.FCM_TOKEN, FirebaseInstanceId.getInstance().getToken());
                                        Intent intent = new Intent(mContext, LoginActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);
                                        finishAffinity();

                                    }
                                }).setCancelable(false).show();
                        }
                    });

                }

            }

            @Override
            public void onFailure(Call<ArrayList<PendingOrderList>> call, Throwable t) {
                System.out.println("getOrderData.onFailure : " + t.getMessage());
                if (t instanceof SocketTimeoutException) {
                    showSnakebare(toolbar, mContext.getString(R.string.time_out), new SnakeBarClick() {
                        @Override
                        public void onItemClick(View view) {
                            getOrderDetailForNotification(orderId);
                        }
                    });
                } else if (t instanceof ConnectException) {
                    showSnakebare(toolbar, mContext.getString(R.string.check_internet), new SnakeBarClick() {
                        @Override
                        public void onItemClick(View view) {

                            getOrderDetailForNotification(orderId);
                        }
                    });
                }
                Utils.progressDialogDismiss();
            }
        });


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        googleMap.getUiSettings().setScrollGesturesEnabled(false);
        googleMap.getUiSettings().setZoomGesturesEnabled(false);
        if (pendingOrderList != null) {
            final LatLng StoreListing = new LatLng(pendingOrderList.getEstablishment().getGeoLocation().getCoordinates().get(1), pendingOrderList.getEstablishment().getGeoLocation().getCoordinates().get(0));
            googleMap.addMarker(new MarkerOptions().position(StoreListing).title(pendingOrderList.getEstablishment().getName())).setIcon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker));
            googleMap.moveCamera(CameraUpdateFactory.newLatLng(StoreListing));
            // Zoom in, animating the camera.
            googleMap.animateCamera(CameraUpdateFactory.zoomIn());

// Zoom out to zoom level 10, animating with a duration of 2 seconds.
            googleMap.animateCamera(CameraUpdateFactory.zoomTo(13), 2000, null);


            googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                @Override
                public void onMapClick(final LatLng latLng) {
                    new AlertDialog.Builder(OrderDetailActivity.this)
                            .setMessage("Open Google Maps?")
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .setPositiveButton("Open", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    String uri = "https://www.google.com/maps/search/?api=1&query=" + StoreListing.latitude + "," + StoreListing.longitude;
                                    Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
                                    intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                                    startActivity(intent);

                                    dialog.dismiss();
                                }
                            }).show();
                }
            });
        }

    }
}
