package com.app.bevvi.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import com.app.bevvi.R;
import com.app.bevvi.activity.MainActivity;
import com.app.bevvi.adapter.ShopListAdapter;
import com.app.bevvi.api.ApiModule;
import com.app.bevvi.api.RetrofitCallback;
import com.app.bevvi.dialog.EnterEmailDialog;
import com.app.bevvi.dialog.NotificationDialog;
import com.app.bevvi.model.responseModel.AccountDetail;
import com.app.bevvi.model.responseModel.StoreListing;
import com.app.bevvi.utils.Const;
import com.app.bevvi.utils.PreferenceManager;
import com.app.bevvi.utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class HomeFragment extends Fragment /*implements LocationDialog.MyClickListner, locationListener.LocationCallback */ {

    /**
     * The Recycler view.
     */
    @BindView(R.id.rvShop)
    RecyclerView recyclerView;
    /**
     * The Toolbar title.
     */
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    /**
     * The Toolbar.
     */
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    /**
     * The Tv home detail.
     */
    @BindView(R.id.tvHomeDetail)
    TextView tvHomeDetail;
    /**
     * The Btn bring to city.
     */
    @BindView(R.id.btnBringToCity)
    AppCompatButton btnBringToCity;
    /**
     * The Cl empty.
     */
    @BindView(R.id.cl_Empty)
    ConstraintLayout clEmpty;
    /**
     * The Cl error.
     */
    @BindView(R.id.cl_error)
    ConstraintLayout cl_error;

    /**
     * The M context.
     */
    Context mContext;
    /**
     * The Unbinder.
     */
    Unbinder unbinder;
    /**
     * The Root view.
     */
    View rootView;

    /**
     * The Shop list adapter.
     */
    ShopListAdapter shopListAdapter;
    /**
     * The List models.
     */
    ArrayList<StoreListing> listModels = new ArrayList<>();
    /**
     * The Preference manager.
     */
    PreferenceManager preferenceManager;


    String city = "", state = "";
    int transit = 0;

    /**
     * The Email dialog.
     */
    AlertDialog emailDialog;
    /**
     * The Notification dialog.
     */
    AlertDialog notificationDialog;
    /**
     * The Request check settings.
     */
//    public Location currentLocation;
    int REQUEST_CHECK_SETTINGS = 100;

    /**
     * Instantiates a new Home fragment.
     */
    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public void onStart() {
        super.onStart();

        System.out.println("HomeFragment.onStart");
    }

    @Override
    public void onResume() {
        super.onResume();
        System.out.println("HomeFragment.onResume");
        try {
            if (!EventBus.getDefault().isRegistered(this)) {
                System.out.println("HomeFragment.onResume : register ");
                EventBus.getDefault().register(this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_home, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        preferenceManager = new PreferenceManager(mContext);
//        locationListner = this;
       /* System.out.println("HomeFragment.onCreateView" + String.format("%.2f", 11.23));
        System.out.println("HomeFragment.onCreateView" + String.format("%.2f", 11.2));
        System.out.println("HomeFragment.onCreateView" + String.format("%.2f", Float.valueOf(123)));*/
        if (preferenceManager.getStringPreference(PreferenceManager.PREF_ADDRESS) != null && !preferenceManager.getStringPreference(PreferenceManager.PREF_ADDRESS).isEmpty())
            setToolbar(preferenceManager.getStringPreference(PreferenceManager.PREF_ADDRESS));

        if (!preferenceManager.getBooleanPreference(PreferenceManager.PREF_Home_FIRST))
            showNotificationDialog();

        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        prepareView();

        getLocationFromDialog();

//        locationListener = new locationListener((AppCompatActivity) mContext, this);

//        loadPermissions(Utils.perm_array);


    }


    private void prepareView() {

        System.out.println("HomeFragment.prepareView TRANSPORT : " + preferenceManager.getIntPreference(PreferenceManager.TRANSPORT));


        shopListAdapter = new ShopListAdapter(getActivity(), listModels, preferenceManager.getIntPreference(PreferenceManager.TRANSPORT), new ShopListAdapter.MyClickListner() {
            @Override
            public void onItemClick(View view, int position) {
                ((MainActivity) getContext()).replaceFragment(getFragmentManager(), R.id.frame, ShopDetailFragment.newInstance(listModels.get(position), listModels.get(position).getId(), listModels.get(position).getName()), Const.ShopDetailFragment);
            }
        });
        recyclerView.setHasFixedSize(false);
        recyclerView.setNestedScrollingEnabled(false);

        LinearLayoutManager rvInvitedLayoutManagaer = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setAdapter(shopListAdapter);
        recyclerView.setLayoutManager(rvInvitedLayoutManagaer);


    }


    /**
     * Gets value from event bus.
     *
     * @param data the data
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getValueFromEventBus(String data) {

        if (data.equalsIgnoreCase("getDataFromLocation")) {

            System.out.println("HomeFragment.getDataFromLocation : ");
            getLocationFromDialog();
//            getShopList(((MainActivity) mContext).currentLocation.getLatitude(), ((MainActivity) mContext).currentLocation.getLongitude());

        } else if (data.equalsIgnoreCase("getShopingList")) {

            System.out.println("HomeFragment.getShopingList : ");
            getShopList(((MainActivity) mContext).currentLocation.getLatitude(), ((MainActivity) mContext).currentLocation.getLongitude());

        } else if (data.equalsIgnoreCase("ConnectException")) {
            System.out.println("HomeFragment.getValueFromEventBus : ConnectException");
            cl_error.setVisibility(View.VISIBLE);

        }
    }

    /**
     * On retry click.
     */
    @OnClick(R.id.btnRetry)
    public void onRetryClick() {
        cl_error.setVisibility(View.GONE);
        EventBus.getDefault().unregister(this);
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.detach(HomeFragment.this).attach(HomeFragment.this).commitAllowingStateLoss();
    }

    private void setToolbar(String address) {

        if (toolbarTitle != null) {
            if (address != null) {
                String[] items = Utils.splitString(address, ",");
                if (items[0].length() > 10)
                    toolbarTitle.setText(items[0]);
                else
                    toolbarTitle.setText(items[0] + "," + items[1]);
            }
            toolbarTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    ((MainActivity) mContext).disconnectGoogleClient();

                    ((MainActivity) mContext).initializeLocationDialog();


                }
            });
        }


    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        System.out.println("HomeFragment.onDestroyView");
        unbinder.unbind();
        EventBus.getDefault().unregister(this);
//        disconnectGoogleClient();
    }


    /**
     * Gets shop list.
     *
     * @param latitude  the latitude
     * @param longitude the longitude
     */
    void getShopList(double latitude, double longitude) {


        Utils.progressDialog(mContext);

        System.out.println("HomeFragment.getShopList lat : lon ==> " +
                preferenceManager.getStringPreference(PreferenceManager.PREF_LAT) +
                " : " + preferenceManager.getStringPreference(PreferenceManager.PREF_LONG));
        double lat = Double.valueOf(preferenceManager.getStringPreference(PreferenceManager.PREF_LAT));
        double lon = Double.valueOf(preferenceManager.getStringPreference(PreferenceManager.PREF_LONG));
        String minDistance = "0";
        double maxDistance = 1 * 1609.34f;
        String limit = "25";


        if (preferenceManager.getLoginData() != null) {
            if (preferenceManager.getStringPreference(PreferenceManager.RADIUS) != null && !TextUtils.isEmpty(preferenceManager.getStringPreference(PreferenceManager.RADIUS))) {
                double radius = Double.valueOf(preferenceManager.getStringPreference(PreferenceManager.RADIUS));
                if (radius > 0)
                    maxDistance = radius * 1609.34f;

                transit = preferenceManager.getIntPreference(PreferenceManager.TRANSPORT);


            }
        }
        tvHomeDetail.setText(R.string.not_available_area);
        btnBringToCity.setBackgroundResource(R.drawable.bg_button_login);
        btnBringToCity.setText("Bring Bevvi to my city");
        btnBringToCity.setEnabled(true);
        btnBringToCity.setClickable(true);

//        System.out.println("HomeFragment.onCreateView : " + filter);
        Call<List<StoreListing>> call = ApiModule.apiService().getStoreListing(lat, lon, latitude, longitude, minDistance, maxDistance, limit, transit);
        call.enqueue(new RetrofitCallback<List<StoreListing>>(mContext, toolbar, new Callback<List<StoreListing>>() {

            @Override
            public void onResponse(Call<List<StoreListing>> call, Response<List<StoreListing>> response) {

                if (Utils.isFragmentVisible(HomeFragment.this)) {
                    if (response.isSuccessful()) {
                        if (response.body().isEmpty()) {
                            recyclerView.setVisibility(View.GONE);
                            clEmpty.setVisibility(View.VISIBLE);
                            Utils.progressDialogDismiss();
                        } else {
                            recyclerView.setVisibility(View.VISIBLE);
                            clEmpty.setVisibility(View.GONE);
                            listModels.clear();
                            listModels.addAll(response.body());
                            shopListAdapter.notifyDataSetChanged();
                            Utils.progressDialogDismiss();
                        }
                    } else {
                        Utils.progressDialogDismiss();
                    }
                }

            }

            @Override
            public void onFailure(Call<List<StoreListing>> call, Throwable t) {
                System.out.println("HomeFragment.onFailure : " + t.getMessage());
                Utils.progressDialogDismiss();

            }
        }));

    }




    /**
     * Gets location from dialog.
     */
    public void getLocationFromDialog() {

        if (preferenceManager.getStringPreference(PreferenceManager.PREF_ADDRESS) != null && !preferenceManager.getStringPreference(PreferenceManager.PREF_ADDRESS).isEmpty())
            setToolbar(preferenceManager.getStringPreference(PreferenceManager.PREF_ADDRESS));

        if (((MainActivity) mContext).currentLocation != null)
            if (Utils.isFragmentVisible(HomeFragment.this))
                getShopList(((MainActivity) mContext).currentLocation.getLatitude(), ((MainActivity) mContext).currentLocation.getLongitude());

    }


    /**
     * On bring to city clicked.
     */
    @OnClick(R.id.btnBringToCity)
    public void onBringToCityClicked() {
        AccountDetail loginResponse = preferenceManager.getAccountDetail(PreferenceManager.ACCOUNT_DETAIL);

        if (loginResponse != null) {
            sendRequestToBringBevvi(loginResponse.getEmail());
        } else {
            emailDialog = new EnterEmailDialog(mContext, true, new EnterEmailDialog.DialogClickListner() {
                @Override
                public void onClickOK(String email) {
                    sendRequestToBringBevvi(email);
                    emailDialog.dismiss();
                    Utils.hideSoftKeyboard(getActivity());
                }

            });
            emailDialog.setCanceledOnTouchOutside(false);
            emailDialog.setCancelable(false);
            emailDialog.show();
            emailDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        }

    }

    private void sendRequestToBringBevvi(String email) {
        Utils.progressDialog(mContext);
        Call<ResponseBody> call = ApiModule.apiService()
                .sendForgotPasswordRequest(email, preferenceManager.getStringPreference(PreferenceManager.PREF_ADDRESS), 1);
        call.enqueue(new RetrofitCallback<ResponseBody>(mContext, toolbar, new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                Utils.progressDialogDismiss();
                if (Utils.isFragmentVisible(HomeFragment.this)) {
                    btnBringToCity.setVisibility(View.INVISIBLE);
                    tvHomeDetail.setText("We will notify you as soon as we are live");
                    btnBringToCity.setBackgroundResource(R.drawable.bg_button_thankyou);
                    btnBringToCity.setText("Thank you!");
                    btnBringToCity.setEnabled(false);
                    btnBringToCity.setClickable(false);
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                Utils.progressDialogDismiss();
                System.out.println("sendForgotPasswordRequest.onFailure : " + t.getMessage());

            }
        }));
    }









    /**
     * Show notification dialog.
     */
    void showNotificationDialog() {
        preferenceManager.setBooleanPreference(PreferenceManager.PREF_Home_FIRST, true);
        notificationDialog = new NotificationDialog(mContext, new NotificationDialog.DialogClickListner() {
            @Override
            public void onClickNotify() {
//                showEmptyDialog();
                notificationDialog.dismiss();
            }

            @Override
            public void onClickNotNow() {
//                showEmptyDialog();
                notificationDialog.dismiss();
            }
        });
        notificationDialog.setCanceledOnTouchOutside(false);
        notificationDialog.setCancelable(false);
        notificationDialog.show();

    }
}
