package com.app.bevvi.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.bevvi.R;
import com.app.bevvi.api.ApiModule;
import com.app.bevvi.dialog.LiquorDetailDialogFragment;
import com.app.bevvi.dialog.LocationDialog;
import com.app.bevvi.dialog.ProductRatingDialog;
import com.app.bevvi.fragment.CartFragment;
import com.app.bevvi.fragment.HomeFragment;
import com.app.bevvi.fragment.ProfileFragment;
import com.app.bevvi.fragment.ProfileLoginFragment;
import com.app.bevvi.model.responseModel.LoginResponse;
import com.app.bevvi.model.responseModel.MyCartList;
import com.app.bevvi.model.responseModel.ProductList;
import com.app.bevvi.model.responseModel.SendLocationResponse;
import com.app.bevvi.utils.Const;
import com.app.bevvi.utils.PreferenceManager;
import com.app.bevvi.utils.Utils;
import com.app.bevvi.utils.locationListener;
import com.appsflyer.AppsFlyerConversionListener;
import com.appsflyer.AppsFlyerLib;
import com.crashlytics.android.Crashlytics;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.fabric.sdk.android.Fabric;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends BaseActivity implements LiquorDetailDialogFragment.MyClickListner, LocationDialog.MyClickListner, locationListener.LocationCallback {


    /**
     * The Navigation.
     */
    @BindView(R.id.navigation)
    BottomNavigationView navigation;
    /**
     * The Frame layout.
     */
    @BindView(R.id.frame)
    FrameLayout frameLayout;


    TextView counterView;
    BottomNavigationItemView itemView;
    public BottomNavigationItemView profileItemView;
    View badge;
    public View pendingBadge;
    Fragment currentFragment;

    /**
     * The Preference manager.
     */
    public PreferenceManager preferenceManager;


    int REQUEST_CHECK_SETTINGS = 100;
    boolean doubleBack = false;
    boolean isCheckCurrentLocation = false;

    locationListener locationListener;
    public LocationDialog locationDialog;
    LocationDialog.MyClickListner locationListner;
    public Location currentLocation;

    public AppCompatActivity appCompatActivity;
    private AlertDialog alertDialog;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:

                    if (!(currentFragment instanceof HomeFragment)) {
                        currentFragment = new HomeFragment();
                        replaceFragment(getSupportFragmentManager(), R.id.frame, currentFragment, Const.HomeFragment);
                    }

//                    viewPager.setCurrentItem(0);
                    return true;
                case R.id.navigation_cart:


                    if (!(currentFragment instanceof CartFragment)) {

                        currentFragment = new CartFragment();
                        replaceFragment(getSupportFragmentManager(), R.id.frame, currentFragment, Const.CartFragment);
                    }

                return true;
                case R.id.navigation_profile:

                    if (preferenceManager.getBooleanPreference(PreferenceManager.ISLOGIN)) {

                        if (!(currentFragment instanceof ProfileFragment)) {

                            currentFragment = new ProfileFragment();
                            replaceFragment(getSupportFragmentManager(), R.id.frame, currentFragment, Const.ProfileFragment);
                        }

                    } else {
                        if (!(currentFragment instanceof ProfileLoginFragment)) {

                            currentFragment = new ProfileLoginFragment();
                            replaceFragment(getSupportFragmentManager(), R.id.frame, currentFragment, Const.ProfileFragment);
                        }

                    }
                    return true;

                default:
                    replaceFragment(getSupportFragmentManager(), R.id.frame, new HomeFragment(), Const.HomeFragment);
                    break;
            }
            return false;
        }
    };


    @Override
    public void onBackPressed() {
        if (doubleBack) {
            super.onBackPressed();
            return;
        }
        doubleBack = true;

        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBack = false;
            }
        }, 2000);

    }


    @Override
    protected void onResume() {
        super.onResume();
        try {
            if (!EventBus.getDefault().isRegistered(this))
                EventBus.getDefault().register(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
//        noInternetDialog.onDestroy();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        Fabric.with(this);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        appCompatActivity = this;

        locationListner = this;
        locationListener = new locationListener(this, this);
        loadPermissions(Utils.perm_array);

        AppsFlyerLib.getInstance().startTracking(this.getApplication(), Const.AF_DEV_KEY);
//        noInternetDialog = new NoInternetDialog.Builder(mContext).build();

        AppsFlyerLib.getInstance().registerConversionListener(this, new AppsFlyerConversionListener() {
            @Override
            public void onInstallConversionDataLoaded(Map<String, String> conversionData) {
                for (String attrName : conversionData.keySet()) {
                    Log.d(AppsFlyerLib.LOG_TAG, "attribute: " + attrName + " = " + conversionData.get(attrName));
                }
                //SCREEN VALUES//
                final String install_type = "Install Type: " + conversionData.get("af_status");
                final String media_source = "Media Source: " + conversionData.get("media_source");
                final String install_time = "Install Time(GMT): " + conversionData.get("install_time");
                final String click_time = "Click Time(GMT): " + conversionData.get("click_time");

                System.out.println("MainActivity.onInstallConversionDataLoaded : " + install_type + "\n" + media_source + "\n" + click_time + "\n" + install_time);
            }

            @Override
            public void onInstallConversionFailure(String errorMessage) {
                Log.d(AppsFlyerLib.LOG_TAG, "error getting conversion data: " + errorMessage);

            }

            @Override
            public void onAppOpenAttribution(Map<String, String> conversionData) {
                System.out.println("MainActivity.onAppOpenAttribution");
            }

            @Override
            public void onAttributionFailure(String errorMessage) {
                Log.d(AppsFlyerLib.LOG_TAG, "error onAttributionFailure : " + errorMessage);
            }
        });

      /*  Map<String, Object> eventValue = new HashMap<String, Object>();
        eventValue.put(AFInAppEventParameterName.REVENUE,200);
        eventValue.put(AFInAppEventParameterName.CONTENT_TYPE,"category_a");
        eventValue.put(AFInAppEventParameterName.CONTENT_ID,"1234567");
        eventValue.put(AFInAppEventParameterName.CURRENCY,"USD");
        AppsFlyerLib.getInstance().trackEvent(mContext, AFInAppEventType.PURCHASE,eventValue);*/


        preferenceManager = new PreferenceManager(this);


        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        BottomNavigationMenuView bottomNavigationMenuView =
                (BottomNavigationMenuView) navigation.getChildAt(0);
        View cartView = bottomNavigationMenuView.getChildAt(1);
        View profileView = bottomNavigationMenuView.getChildAt(2);
        itemView = (BottomNavigationItemView) cartView;
        profileItemView = (BottomNavigationItemView) profileView;

        badge = LayoutInflater.from(this)
                .inflate(R.layout.badge_count, bottomNavigationMenuView, false);

        pendingBadge = LayoutInflater.from(this)
                .inflate(R.layout.badge_profile, bottomNavigationMenuView, false);

        counterView = badge.findViewById(R.id.tvNotifications_badge);
        counterView.setText("0");

        ArrayList<MyCartList> cartList = preferenceManager.getCartData(mContext, PreferenceManager.CART_DATA);
        if (cartList != null && cartList.size() > 0) {
            int remQty = 0;
            for (int i = 0; i < cartList.size(); i++) {
                remQty = remQty + cartList.get(i).getQuantity();
            }
            counterView.setText(String.valueOf(remQty));
        }

        if (Integer.valueOf(counterView.getText().toString()) > 0) {
            if (counterView.getText().toString().length() < 2) {
                counterView.setPadding(12, 3, 12, 3);
            }

            itemView.addView(badge);
        }


        if (!preferenceManager.getBooleanPreference(PreferenceManager.PREF_ISFISTTIME)) {

//            navigation.setVisibility(View.GONE);
            preferenceManager.setBooleanPreference(PreferenceManager.PREF_ISFISTTIME, true);
            startActivity(new Intent(this, AskLocationEnableActivity.class));
        } else {
//            navigation.setVisibility(View.VISIBLE);
            currentFragment = new HomeFragment();
            replaceFragment(getSupportFragmentManager(), R.id.frame, currentFragment, Const.HomeFragment);
        }

//        setupViewPager();


        if (getIntent().hasExtra("itemAvailableForPickup")) {

            if (getIntent().getExtras() != null && getIntent().getExtras().getBoolean("itemAvailableForPickup")) {

                profileItemView.removeView(pendingBadge);
                profileItemView.addView(pendingBadge);

            } else
                profileItemView.removeView(pendingBadge);
        }


        getCartList();


    }

    /**
     * Load permissions.
     *
     * @param perm_array the perm array
     */
    public void loadPermissions(String[] perm_array) {
        System.out.println("MainActivity Load permission : ");
        ((MainActivity) mContext).isCheckCurrentLocation = true;
        ArrayList<String> permArray = new ArrayList<>();
        for (String permission : perm_array) {
            if (ContextCompat.checkSelfPermission(mContext, permission) != PackageManager.PERMISSION_GRANTED) {
                permArray.add(permission);
            }
        }
        perm_array = new String[permArray.size()];
        perm_array = permArray.toArray(perm_array);

        if (perm_array.length > 0) {
            ActivityCompat.requestPermissions(this, perm_array, 0);
        } else {
            if (Utils.isConnectedToInternet(mContext)) {
                if (locationListener.isPlayServiceAvailable(mContext)) {
                    disconnectGoogleClient();
                    try {
                        Utils.progressDialog(mContext);
                        locationListener.setGoogleClient(mContext);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else
                EventBus.getDefault().post("ConnectException");

        }
    }

    @Override
    public void onPause() {
        super.onPause();
        disconnectGoogleClient();

        System.out.println("HomeFragment.onPause");
    }

    /**
     * Replace fragment.
     *
     * @param fragmentManager the fragment manager
     * @param containerId     the container id
     * @param fragment        the fragment
     * @param tag             the tag
     */
    public void replaceFragment(FragmentManager fragmentManager, int containerId, Fragment fragment, String tag) {

        fragmentManager.beginTransaction().replace(containerId, fragment, tag)/*.addToBackStack(tag)*/.commit();
    }


    @Override
    protected void onStop() {
        super.onStop();

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {

            case 0:
                boolean isDenied = false;
                for (int i = 0; i < grantResults.length; i++) {
                    System.out.println(grantResults[i]);
                    if (grantResults[i] == -1) {
                        isDenied = true;
                    }
                }

                if (!isDenied) {
                    try {
                        EventBus.getDefault().post("getLocation");
                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(this, "Permission Denied", Toast.LENGTH_LONG).show();
                    EventBus.getDefault().post("PermissionDenied");
//                    finish();
                }
                break;
        }
    }

    private void getCartList() {
//        {"where":{"accountId":"5a3c9c86912c756d59ce844d","state":0},"include":["product","offer"]}
        final LoginResponse loginResponse = preferenceManager.getLoginData();

        if (loginResponse != null) {

            JsonObject object = new JsonObject();
            JsonObject where = new JsonObject();
            where.addProperty("accountId", loginResponse.getUserId());
            where.addProperty("state", "0");
            object.add("where", where);

            JsonArray include = new JsonArray();
            include.add("product");

            JsonArray array = new JsonArray();
            array.add("establishment");
            JsonObject offerObject = new JsonObject();
            offerObject.add("offer", array);
            include.add(offerObject);

            object.add("include", include);
            System.out.println("LiquorDetailFragment.getCartList : " + object);
//            mDeviceBandwidthSampler.startSampling();
            Call<List<MyCartList>> call = ApiModule.apiService().getCartListing(object.toString());

            call.enqueue(new Callback<List<MyCartList>>() {

                @Override
                public void onResponse(Call<List<MyCartList>> call, Response<List<MyCartList>> response) {
                    if (response.isSuccessful()) {
//                        mDeviceBandwidthSampler.stopSampling();

                        int totalQty = 0;

                        for (int i = 0; i < response.body().size(); i++) {
                            totalQty = totalQty + response.body().get(i).getQuantity();
                        }
                        System.out.println("LiquorDetailFragment.onResponse totalQty : " + totalQty);

                        setBadgetCount(totalQty);


                    } else
                        System.out.println("LiquorDetailFragment.onResponse : success false");

                    if (!Const.isRattingAPICALL) {
                        loadProductRating(loginResponse.getUserId(), loginResponse.getAccessToken());
                    }
                }

                @Override
                public void onFailure(Call<List<MyCartList>> call, Throwable t) {
                    System.out.println("LiquorDetailFragment.onFailure : " + t.getMessage());
                    Utils.progressDialogDismiss();

                }
            });

        } else if (preferenceManager.getCartData(mContext, PreferenceManager.CART_DATA) != null && preferenceManager.getCartData(mContext, PreferenceManager.CART_DATA).size() > 0) {

            int totalQty = 0;

            for (int i = 0; i < preferenceManager.getCartData(mContext, PreferenceManager.CART_DATA).size(); i++) {
                totalQty = totalQty + preferenceManager.getCartData(mContext, PreferenceManager.CART_DATA).get(i).getQuantity();
            }
            System.out.println("LiquorDetailFragment.onResponse else if totalQty : " + totalQty);

            setBadgetCount(totalQty);
        }
    }

    private void loadProductRating(String userId, String accessToken) {

        Call<ArrayList<ProductList>> call = ApiModule.apiService().getRatingProducts(userId, accessToken);
        call.enqueue(new Callback<ArrayList<ProductList>>() {

            @Override
            public void onResponse(@NonNull Call<ArrayList<ProductList>> call, @NonNull Response<ArrayList<ProductList>> response) {
                if (response.isSuccessful()) {

                    Const.isRattingAPICALL = true;

                    if (!response.body().isEmpty()) {
                        alertDialog = new ProductRatingDialog(mContext, null, response.body(), new ProductRatingDialog.DialogClickListner() {

                            @Override
                            public void onClickDismissButton() {
                                alertDialog.dismiss();
                            }

                            @Override
                            public void onClickSubmitButton() {
                                alertDialog.dismiss();
                            }
                        });
                        alertDialog.show();
                    }

                } else if (response.raw().code() == 401) {
                    Utils.progressDialogDismiss();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (!isFinishing()) {

                                new AlertDialog.Builder(mContext).setTitle("Alert").setMessage(R.string.session_expire).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        PreferenceManager preferenceManager = new PreferenceManager(mContext);
                                        preferenceManager.clearPrefrences();
                                        preferenceManager.setStringPreference(PreferenceManager.FCM_TOKEN, FirebaseInstanceId.getInstance().getToken());
                                        Intent intent = new Intent(mContext, LoginActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        finish();
                                        startActivity(intent);
                                        finishAffinity();

                                    }
                                }).setCancelable(false).show();

                            }
                        }
                    });

                }


            }

            @Override
            public void onFailure(Call<ArrayList<ProductList>> call, Throwable t) {
                System.out.println("loadProductRating.onFailure : " + t.getMessage());
                Utils.progressDialogDismiss();

            }
        });

    }

    private void setBadgetCount(int totalQty) {
        if (totalQty > 0) {
            JsonArray elements = new JsonArray();
            elements.add("cartLists");
            elements.add(totalQty);
            EventBus.getDefault().post(elements);
        } else {
            EventBus.getDefault().post("cartListsBlank");
        }
    }

    /**
     * Gets cart list.
     *
     * @param data the data
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getCartList(JsonArray data) {
        System.out.println("MainActivity.getCartList");
        if (data.get(0).getAsString().equals("cartLists")) {
            if (data.get(1).getAsInt() > 0) {
                if (counterView.getVisibility() == View.GONE)
                    counterView.setVisibility(View.VISIBLE);
                counterView.setText(data.get(1).getAsString());
            }

            if (Integer.valueOf(counterView.getText().toString()) > 0) {
                if (counterView.getText().toString().length() < 2) {
                    counterView.setPadding(12, 3, 12, 3);
                }
                itemView.removeView(badge);
                itemView.addView(badge);
            }
        }


    }

    /**
     * Gets value from event bus.
     *
     * @param data the data
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getValueFromEventBus(String data) {
        System.out.println("MainActivity.getValueFromEventBus : " + data);
        if (data.equalsIgnoreCase("cartListsBlank")) {

            itemView.removeView(badge);
            counterView.setVisibility(View.GONE);
        } else if (data.equalsIgnoreCase("getValueFromSearch")) {
            double lat = Double.valueOf(preferenceManager.getStringPreference(PreferenceManager.PREF_LAT));
            double lon = Double.valueOf(preferenceManager.getStringPreference(PreferenceManager.PREF_LONG));
            sendSearchLocation(lat, lon, preferenceManager.getStringPreference(PreferenceManager.PREF_ADDRESS));
            locationDialog.dismissDialog();

        } else if (data.equalsIgnoreCase("getLocation")) {
            System.out.println("MainActivity.getValueFromEventBus : getLocation");
            if (locationListener.isPlayServiceAvailable(mContext)) {
                System.out.println("MainActivity.getValueFromEventBus : isPlayServiceAvailable");
                disconnectGoogleClient();
                locationListener.setGoogleClient(mContext);
            }
        } else if (data.equalsIgnoreCase("PermissionDenied")) {

            disconnectGoogleClient();
            System.out.println("MainActivity.getValueFromEventBus : PermissionDenied");
            loadPermissions(Utils.perm_array);
        } else if (data.equalsIgnoreCase("locationEnable")) {
            System.out.println("MainActivity.getValueFromEventBus : locationEnable");
            loadPermissions(Utils.perm_array);

        }


    }


    private void sendSearchLocation(final double lat, final double lon, final String address) {

        /*{
            "geoLocation" : {
                    "type" : "Point",
                    "coordinates" : [
                    -118.4109089,
                    33.884736100000005
                    ]
                }
        }*/

        JsonObject jsonObject = new JsonObject();
        JsonObject geoLocation = new JsonObject();

        JsonArray elements = new JsonArray();
        elements.add(lon);
        elements.add(lat);

        geoLocation.add("coordinates", elements);
        geoLocation.addProperty("type", "Point");

        jsonObject.add("geoLocation", geoLocation);

        Utils.progressDialog(mContext);
        Call<SendLocationResponse> call = ApiModule.apiService().sendSearchLocation(jsonObject);
        call.enqueue(new Callback<SendLocationResponse>() {

            @Override
            public void onResponse(Call<SendLocationResponse> call, Response<SendLocationResponse> response) {

                Utils.progressDialogDismiss();

                if (response.isSuccessful()) {
                    LoginResponse loginResponse = preferenceManager.getLoginData();
                    if (loginResponse != null)
                        addLocationToSearchHistory(loginResponse.getUserId(), lat, lon, response.body().getId(), address);

                }
                EventBus.getDefault().post("getDataFromLocation");

            }

            @Override
            public void onFailure(Call<SendLocationResponse> call, Throwable t) {
                System.out.println("sendSearchLocation.onFailure : " + t.getMessage());
                Utils.progressDialogDismiss();

                if (t instanceof SocketTimeoutException) {
                    EventBus.getDefault().post("ConnectException");
                    showSnakebare(navigation, mContext.getString(R.string.time_out));
                } else if (t instanceof ConnectException) {
                    EventBus.getDefault().post("ConnectException");
                    showSnakebare(navigation, mContext.getString(R.string.time_out));
                } else if (t instanceof UnknownHostException) {
                    EventBus.getDefault().post("ConnectException");
//                    showSnakebare(navigation,mContext.getString(R.string.time_out));
                }

            }
        });


    }

    private void addLocationToSearchHistory(String userID, double lat, double lon, String locationId, String address) {
        /*{
            "accountId" : "5a5dd89ace6f114067cf8889",
                "geoLocation" : {
                   "type" : "Point",
                    "coordinates" : [
                     -118.4109089,
                    33.884736100000005
                  ]
            },
            "locationId" : "5a5ee4214dc5534f69f732f7",
                "address" : "Manhattan Beach, CA, USA"
        }*/

        JsonObject jsonObject = new JsonObject();
        JsonObject geoLocation = new JsonObject();

        JsonArray elements = new JsonArray();
        elements.add(lon);
        elements.add(lat);

        geoLocation.add("coordinates", elements);
        geoLocation.addProperty("type", "Point");

        jsonObject.addProperty("accountId", userID);
        jsonObject.add("geoLocation", geoLocation);
        jsonObject.addProperty("locationId", locationId);
        jsonObject.addProperty("address", address);


        Call<ResponseBody> call = ApiModule.apiService().addLocationToSearchHistory(jsonObject);
        call.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

//                Utils.progressDialogDismiss();

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                System.out.println("HomeFragment.onFailure : " + t.getMessage());
                Utils.progressDialogDismiss();

                if (t instanceof SocketTimeoutException) {
                    EventBus.getDefault().post("ConnectException");
                    showSnakebare(navigation, mContext.getString(R.string.time_out));
                } else if (t instanceof ConnectException) {
                    EventBus.getDefault().post("ConnectException");
                    showSnakebare(navigation, mContext.getString(R.string.time_out));
                } else if (t instanceof UnknownHostException) {
                    EventBus.getDefault().post("ConnectException");
//                    showSnakebare(navigation,mContext.getString(R.string.time_out));
                }

            }
        });


    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        System.out.println("MainActivity.onActivityResult resultCode: " + resultCode);
        if (requestCode == REQUEST_CHECK_SETTINGS) {
            System.out.println("MainActivity.onActivityResult : ");

            System.out.println("MainActivity.onActivityResult inside resultCode: " + resultCode);

            EventBus.getDefault().post("locationEnable");

            if (resultCode == Activity.RESULT_OK) {
                Toast.makeText(mContext, "GPS enabled", Toast.LENGTH_LONG).show();
//                disconnectGoogleClient();
                locationListener.requestLocations();
                //startHandler();
            } else {
                Toast.makeText(mContext, "Location get", Toast.LENGTH_LONG).show();
//                disconnectGoogleClient();
                locationListener.requestSettings();
            }


        }

    }

    @Override
    public void onItemUpdate() {

        currentFragment = new CartFragment();
        replaceFragment(getSupportFragmentManager(), R.id.frame, currentFragment, Const.CartFragment);

    }


    @Override
    public void locationFound(Location location) {

        currentLocation = location;
        preferenceManager.setStringPreference(PreferenceManager.PREF_CURRENT_LAT, String.valueOf(location.getLatitude()));
        preferenceManager.setStringPreference(PreferenceManager.PREF_CURRENT_LONG, String.valueOf(location.getLongitude()));
        if (!preferenceManager.getStringPreference(PreferenceManager.PREF_ADDRESS).isEmpty()) {
            EventBus.getDefault().post("getShopingList");
        } else {
            disconnectGoogleClient();

            initializeLocationDialog();
        }

    }

    /**
     * Initialize location dialog.
     */
    public void initializeLocationDialog() {
        locationDialog = new LocationDialog(mContext, this, null, this);
    }

    /**
     * Disconnect google client.
     */
    public void disconnectGoogleClient() {
        if (locationListener != null && locationListener.mGoogleApiClient != null && locationListener.mGoogleApiClient.isConnected()) {
            locationListener.mGoogleApiClient.stopAutoManage((MainActivity) mContext);
            locationListener.mGoogleApiClient.disconnect();
        }
        if (locationDialog != null && locationDialog.mGoogleApiClient != null && locationDialog.mGoogleApiClient.isConnected()) {
            locationDialog.mGoogleApiClient.stopAutoManage((MainActivity) mContext);
            locationDialog.mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onGetLocation(boolean isForSearch) {
        if (isForSearch) {
            sendSearchLocation(Double.valueOf(preferenceManager.getStringPreference(PreferenceManager.PREF_LAT)), Double.valueOf(preferenceManager.getStringPreference(PreferenceManager.PREF_LONG)), preferenceManager.getStringPreference(PreferenceManager.PREF_ADDRESS));
        } else {
            EventBus.getDefault().post("getDataFromLocation");
            locationDialog.dismissDialog();
        }
    }
}
