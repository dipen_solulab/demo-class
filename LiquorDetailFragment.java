package com.app.bevvi.fragment;


import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.bevvi.R;
import com.app.bevvi.activity.LoginActivity;
import com.app.bevvi.activity.MainActivity;
import com.app.bevvi.api.ApiModule;
import com.app.bevvi.api.RetrofitCallback;
import com.app.bevvi.dialog.EmptyCartDialog;
import com.app.bevvi.model.cartModel.CartEstablishment;
import com.app.bevvi.model.cartModel.CartGeoLocation;
import com.app.bevvi.model.cartModel.CartOffer;
import com.app.bevvi.model.cartModel.CartProduct;
import com.app.bevvi.model.responseModel.AddToCartResponse;
import com.app.bevvi.model.responseModel.LoginResponse;
import com.app.bevvi.model.responseModel.MyCartList;
import com.app.bevvi.model.responseModel.ProductListing;
import com.app.bevvi.model.responseModel.StoreListing;
import com.app.bevvi.utils.Const;
import com.app.bevvi.utils.PreferenceManager;
import com.app.bevvi.utils.Utils;
import com.bumptech.glide.Glide;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.app.bevvi.api.ApiModule.IMAGE_BASE_URL;

public class LiquorDetailFragment extends Fragment {


    View rootView;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar_liquorDetail)
    Toolbar toolbar;
    @BindView(R.id.lq_detail_ivProduct)
    ImageView lqDetailIvProduct;
    @BindView(R.id.lq_detail_tvName)
    TextView lqDetailTvName;
    @BindView(R.id.lq_detail_tvType)
    TextView lqDetailTvType;
    @BindView(R.id.lq_detail_rating)
    AppCompatRatingBar lqDetailRating;
    @BindView(R.id.lq_detail_tvStock)
    TextView lqDetailTvStock;
    @BindView(R.id.lq_detail_tvOfferTime)
    TextView lqDetailTvOfferTime;
    @BindView(R.id.lq_detail_tvNewPrice)
    TextView lqDetailTvNewPrice;
    @BindView(R.id.lq_detail_tvOldPrice)
    TextView lqDetailTvOldPrice;
    @BindView(R.id.lq_detail_tvSize)
    TextView lqDetailTvSize;
    @BindView(R.id.lq_detail_tvContent)
    TextView lqDetailTvContent;
    @BindView(R.id.lq_detail_tvRegion)
    TextView lqDetailTvRegion;
    @BindView(R.id.lq_detail_tvCountry)
    TextView lqDetailTvCountry;
    @BindView(R.id.lq_detail_tvDetail)
    TextView lqDetailTvDetail;
    @BindView(R.id.tvAddtoCart)
    TextView tvAddtoCart;
    @BindView(R.id.lq_detail_ivMinus)
    ImageView lqDetailIvMinus;
    @BindView(R.id.lq_detail_tvCounter)
    TextView lqDetailTvCounter;
    @BindView(R.id.lq_detail_ivAdd)
    ImageView lqDetailIvAdd;
    @BindView(R.id.lq_detail_llCart)
    LinearLayout lqDetailLlCart;
    @BindView(R.id.tvSizeTitle)
    TextView tvSizeTitle;
    @BindView(R.id.tvALCTitle)
    TextView tvALCTitle;
    @BindView(R.id.tvRegionTitle)
    TextView tvRegionTitle;
    @BindView(R.id.tvTotalAmount)
    TextView tvTotalAmount;
    @BindView(R.id.tvYearTitle)
    TextView tvYearTitle;
    @BindView(R.id.tvYear)
    TextView tvYear;
    @BindView(R.id.tvVarietalTitle)
    TextView tvVarietalTitle;
    @BindView(R.id.tvVarietal)
    TextView tvVarietal;
    @BindView(R.id.cl_error)
    ConstraintLayout cl_error;
    @BindView(R.id.llSize)
    LinearLayout llSize;
    @BindView(R.id.llALC)
    LinearLayout llALC;
    @BindView(R.id.llRow1)
    LinearLayout llRow1;
    @BindView(R.id.llRegion)
    LinearLayout llRegion;
    @BindView(R.id.llCountry)
    LinearLayout llCountry;
    @BindView(R.id.llRow2)
    LinearLayout llRow2;
    @BindView(R.id.llYear)
    LinearLayout llYear;
    @BindView(R.id.llVarietal)
    LinearLayout llVarietal;
    @BindView(R.id.llRow3)
    LinearLayout llRow3;


    Context mContext;

    PreferenceManager preferenceManager;

    ProductListing productListing;
    StoreListing storeListings;

    ArrayList<MyCartList> cartLists = new ArrayList<>();

    Unbinder unbinder;

    String Tooltitle;
    int itemCount = 1;

    AlertDialog emptyCartDialog;


    public LiquorDetailFragment() {
        // Required empty public constructor
    }

    public static LiquorDetailFragment newInstance(StoreListing storeListings, ProductListing productlisting, String title) {
        LiquorDetailFragment fragment = new LiquorDetailFragment();
        Bundle args = new Bundle();
        args.putSerializable("productlisting", productlisting);
        args.putSerializable("storeListings", storeListings);
        args.putString("title", title);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        try {
            if (!EventBus.getDefault().isRegistered(this))
                EventBus.getDefault().register(this);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        preferenceManager = new PreferenceManager(mContext);
        if (getArguments() != null) {
            productListing = (ProductListing) getArguments().getSerializable("productlisting");
            storeListings = (StoreListing) getArguments().getSerializable("storeListings");
            Tooltitle = getArguments().getString("title");

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_liquor_detail, container, false);
        unbinder = ButterKnife.bind(this, rootView);

        cartLists = preferenceManager.getCartData(mContext, PreferenceManager.CART_DATA);

        prepareView();
        return rootView;
    }

    private void prepareView() {

        CartProduct product = productListing.getProduct();
        lqDetailTvName.setText(product.getName());
        lqDetailTvType.setText(product.getCategory());

        setProductDetail(product);

        if (product.getRating() != null)
            lqDetailRating.setRating(Float.valueOf(product.getRating()));
        else
            lqDetailRating.setRating(0f);

        if (productListing.getProduct().getProductPhoto() != null && productListing.getProduct().getProductPhoto().getFile() != null)
            Glide.with(mContext).load(IMAGE_BASE_URL + productListing.getProduct().getProductPhoto().getFile().getFilename())/*.centerCrop()*/.placeholder(R.drawable.image_loading).error(R.drawable.no_image).into(lqDetailIvProduct);

        lqDetailTvSize.setText(product.getSize() + product.getUnits());
        lqDetailTvStock.setText(productListing.getRemQty() + " out " + productListing.getTotalQty() + " bottles left");

        if (productListing.getOriginalPrice().equals(productListing.getSalePrice())) {
            lqDetailTvOldPrice.setVisibility(View.INVISIBLE);
        }
        lqDetailTvOldPrice.setText("$" + productListing.getOriginalPrice());

        lqDetailTvNewPrice.setText("$" + productListing.getSalePrice());

        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

        String formattedDate = df.format(c);

        lqDetailTvOfferTime.setText("Offer expires in " + Utils.getDaysDiffrance("yyyy-MM-dd", formattedDate, productListing.getEndsAt()) + " days");
        setToolbar(product.getName());

        if (productListing.getRemQty() <= 0) {
            tvAddtoCart.setText("OUT OF STOCK");
            lqDetailTvCounter.setText("0");
            tvAddtoCart.setClickable(false);
            tvAddtoCart.setEnabled(false);
            lqDetailIvAdd.setClickable(false);
            lqDetailIvAdd.setEnabled(false);
            lqDetailIvMinus.setClickable(false);
            lqDetailIvMinus.setEnabled(false);

        }
    }

    private void setProductDetail(CartProduct product) {


        if (product.getAlcContent() != null && !product.getAlcContent().isEmpty())
            setValue(lqDetailTvContent, product.getAlcContent() + " Proof");
        else
            setValue(lqDetailTvContent, "N/A");

        if (product.getCountry() != null && !product.getCountry().isEmpty())
            setValue(lqDetailTvCountry, product.getCountry());
        else
            setValue(lqDetailTvCountry, "N/A");

        if (product.getDescription() != null && !product.getDescription().isEmpty())
            setValue(lqDetailTvDetail, product.getDescription());
        else
            setValue(lqDetailTvDetail, "N/A");

        if (product.getRegion() != null && !product.getRegion().isEmpty())
            setValue(lqDetailTvRegion, product.getRegion());
        else
            setValue(lqDetailTvRegion, "N/A");

        if (product.getYear() != null && !product.getYear().isEmpty())
            setValue(tvYear, product.getYear());
        else
            setValue(tvYear, "N/A");

        if (product.getVarietal() != null && !product.getVarietal().isEmpty())
            setValue(tvVarietal, product.getVarietal());
        else
            setValue(tvVarietal, "N/A");

    }

    private void setValue(TextView textView, String value) {

        textView.setText(value);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick(R.id.lq_detail_ivProduct)
    void onClickProductImage() {

        if (productListing.getProduct().getProductPhoto() != null && productListing.getProduct().getProductPhoto().getFile() != null)
            showPreviewImage(productListing.getProduct().getProductPhoto().getFile().getFilename(), "");

    }

//    AlertDialog addToCartDialog;


    @OnClick(R.id.tvAddtoCart)
    void onClickAddtoCart() {

        if (!preferenceManager.getBooleanPreference(PreferenceManager.ISLOGIN)) {
            if (cartLists != null) {
                cartLists.clear();
                cartLists = preferenceManager.getCartData(mContext, PreferenceManager.CART_DATA);
                if (cartLists.size() > 0) {
                    if (cartLists.get(0).getOffer().getEstablishmentId().equalsIgnoreCase(productListing.getEstablishmentId())) {


                        Collections.sort(cartLists);
                        int index = Collections.binarySearch(cartLists, new MyCartList(productListing.getProductId()));
                        if (index >= 0)
                            addItemInExistingArray(true, index);
                        else
                            addItemInExistingArray(false, 0);

                       /* for (int i = 0; i < cartLists.size(); i++) {
                            System.out.println("LiquorDetailFragment productId comapre : " + cartLists.get(i).getProductId() + " --> " + productListing.getProductId());
                            if (cartLists.get(i).getProductId().equalsIgnoreCase(productListing.getProductId())) {
                                addItemInExistingArray(true, i);
                                break;
                            } else {
                                addItemInExistingArray(false, 0);
                                break;
                            }

                        }*/

                    } else {
                        System.out.println("LiquorDetailFragment.onClickAddtoCart : id not match show empty cart dialog");
                        showEmptyCartDialog(false);
                    }
                } else {
                    cartLists = new ArrayList<>();
                    addNewItemTocart();
                }
            } else {
                cartLists = new ArrayList<>();
                addNewItemTocart();

            }

            saveCartToPref(cartLists);


        } else {
            if (preferenceManager.getStringPreference(PreferenceManager.ESTABLISHMENTID).isEmpty()
                    || preferenceManager.getStringPreference(PreferenceManager.ESTABLISHMENTID)
                    .equalsIgnoreCase(productListing.getEstablishmentId())) {

                Utils.progressDialog(mContext);
                addItemToCart();


            } else {
                System.out.println("LiquorDetailFragment.onClickAddtoCart : id not match show empty cart dialog");
                showEmptyCartDialog(true);
            }
        }

    }

    void updatButtonText() {
        final Handler handler = new Handler();
        Timer t = new Timer();
        t.schedule(new TimerTask() {
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        //THIS ACTIONS WILL WILL EXECUTE AFTER 3 SECONDS...
                        if (Utils.isFragmentVisible(LiquorDetailFragment.this)) {
                            tvAddtoCart.setClickable(true);
                            tvAddtoCart.setEnabled(true);
                            lqDetailIvAdd.setClickable(true);
                            lqDetailIvAdd.setEnabled(true);
                            itemCount = 1;
                            lqDetailTvCounter.setText("1");
                            tvAddtoCart.setText("UPDATE CART");
                            lqDetailLlCart.setBackgroundResource(R.drawable.bg_button_login);
                            lqDetailTvCounter.setBackgroundResource(R.drawable.bg_button_login_dark);
                        }
                    }
                });
            }
        }, 2000);
    }

    private void addItemToCart() {
        LoginResponse loginResponse = preferenceManager.getLoginData();
        Call<AddToCartResponse> call = ApiModule.apiService()
                .addItemToCart(loginResponse.getUserId(), itemCount, productListing.getProductId(),
                        productListing.getId(), 0, loginResponse.getAccessToken());
        call.enqueue(new RetrofitCallback<AddToCartResponse>(mContext, toolbar, new Callback<AddToCartResponse>() {

            @Override
            public void onResponse(Call<AddToCartResponse> call, Response<AddToCartResponse> response) {

                System.out.println("doLogin.onResponse : doLogin ");
//                Utils.makeToast(mContext, "Item Added successfully");
                Utils.progressDialogDismiss();
                if (response.isSuccessful()) {
                    if (preferenceManager.getStringPreference(PreferenceManager.ESTABLISHMENTID) != null) {
                        preferenceManager.setStringPreference(PreferenceManager.ESTABLISHMENTID, productListing.getEstablishmentId());
                    }
//                    tvAddtoCart.setClickable(false);
//                    tvAddtoCart.setEnabled(false);
                    lqDetailLlCart.setBackgroundResource(R.drawable.bg_button_login_dark);
                    lqDetailTvCounter.setBackgroundResource(R.drawable.bg_button_login);
                    if (tvAddtoCart.getText().toString().equalsIgnoreCase("UPDATE CART")) {
                        tvAddtoCart.setText("UPDATED");
                    } else {
                        tvAddtoCart.setText("ADDED");
                    }

                    lqDetailIvAdd.setClickable(false);
                    lqDetailIvAdd.setEnabled(false);
                    tvAddtoCart.setClickable(false);
                    tvAddtoCart.setEnabled(false);
                    updatButtonText();
                    getCartList();


                } else if (response.raw().code() == 401) {

                    ((AppCompatActivity) mContext).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (!((AppCompatActivity) mContext).isFinishing()) {

                                new AlertDialog.Builder(mContext).setTitle("Alert").setMessage(R.string.session_expire).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        PreferenceManager preferenceManager = new PreferenceManager(mContext);
                                        preferenceManager.clearPrefrences();
                                        preferenceManager.setStringPreference(PreferenceManager.FCM_TOKEN, FirebaseInstanceId.getInstance().getToken());
                                        Intent intent = new Intent(mContext, LoginActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        ((AppCompatActivity) mContext).finish();
                                        startActivity(intent);
                                        ((AppCompatActivity) mContext).finishAffinity();

                                    }
                                }).setCancelable(false).show();

                            }
                        }
                    });

                }
            }

            @Override
            public void onFailure(Call<AddToCartResponse> call, Throwable t) {

                Utils.progressDialogDismiss();
                System.out.println("LiquorDetailFragment.onFailure : " + t.getMessage());

            }
        }));
    }

    /*  private void getCartList() {

          JsonArray elements = new JsonArray();
          elements.add("cartLists");
          elements.add(1);
          EventBus.getDefault().post(elements);
      }
  */
    private void getCartList() {
//        {"where":{"accountId":"5a3c9c86912c756d59ce844d","state":0},"include":["product","offer"]}
        LoginResponse loginResponse = preferenceManager.getLoginData();

        if (loginResponse != null) {

            JsonObject object = new JsonObject();
            JsonObject where = new JsonObject();
            where.addProperty("accountId", loginResponse.getUserId());
            where.addProperty("state", "0");
            object.add("where", where);

            JsonArray include = new JsonArray();
            include.add("product");

            JsonArray array = new JsonArray();
            array.add("establishment");
            JsonObject offerObject = new JsonObject();
            offerObject.add("offer", array);
            include.add(offerObject);

            object.add("include", include);
            System.out.println("LiquorDetailFragment.getCartList : " + object);
            Call<List<MyCartList>> call = ApiModule.apiService().getCartListing(object.toString());

            call.enqueue(new RetrofitCallback<List<MyCartList>>(mContext, toolbar, new Callback<List<MyCartList>>() {

                @Override
                public void onResponse(Call<List<MyCartList>> call, Response<List<MyCartList>> response) {
                    if (response.isSuccessful()) {

                        int totalQty = 0;

                        for (int i = 0; i < response.body().size(); i++) {
                            totalQty = totalQty + response.body().get(i).getQuantity();
                        }
                        System.out.println("LiquorDetailFragment.onResponse totalQty : " + totalQty);

                        setBadgetCount(totalQty);


                    } else
                        System.out.println("LiquorDetailFragment.onResponse : success false");
                }

                @Override
                public void onFailure(Call<List<MyCartList>> call, Throwable t) {
                    System.out.println("LiquorDetailFragment.onFailure : " + t.getMessage());
                    Utils.progressDialogDismiss();

                }
            }));

        } else if (preferenceManager.getCartData(mContext, PreferenceManager.CART_DATA) != null && preferenceManager.getCartData(mContext, PreferenceManager.CART_DATA).size() > 0) {

            int totalQty = 0;

            for (int i = 0; i < preferenceManager.getCartData(mContext, PreferenceManager.CART_DATA).size(); i++) {
                totalQty = totalQty + preferenceManager.getCartData(mContext, PreferenceManager.CART_DATA).get(i).getQuantity();
            }
            System.out.println("LiquorDetailFragment.onResponse else if totalQty : " + totalQty);

            setBadgetCount(totalQty);
        }
    }

    private void setBadgetCount(int totalQty) {
        if (totalQty > 0) {
            JsonArray elements = new JsonArray();
            elements.add("cartLists");
            elements.add(totalQty);
            EventBus.getDefault().post(elements);
        } else {
            EventBus.getDefault().post("cartListsBlank");
        }
    }

    private void saveCartToPref(ArrayList<MyCartList> cartLists) {
        Gson gson = new Gson();
        String cartData = gson.toJson(cartLists);
        System.out.println("LiquorDetailFragment.onClickAddtoCart : cartData " + cartData);
        preferenceManager.setStringPreference(PreferenceManager.CART_DATA, cartData);

        int totalQty = 0;

        for (int i = 0; i < cartLists.size(); i++) {
            totalQty = totalQty + cartLists.get(i).getQuantity();
        }
        System.out.println("saveCartToPref totalQty : " + totalQty);

        setBadgetCount(totalQty);


        lqDetailLlCart.setBackgroundResource(R.drawable.bg_button_login_dark);
        lqDetailTvCounter.setBackgroundResource(R.drawable.bg_button_login);
        if (tvAddtoCart.getText().toString().equalsIgnoreCase("UPDATE CART"))
            tvAddtoCart.setText("UPDATED");
        else
            tvAddtoCart.setText("ADDED");
        lqDetailIvAdd.setClickable(false);
        lqDetailIvAdd.setEnabled(false);
        tvAddtoCart.setClickable(false);
        tvAddtoCart.setEnabled(false);
        updatButtonText();
        getCartList();
    }

    private void addItemInExistingArray(boolean isReplace, int index) {


        CartProduct addToCart = productListing.getProduct();


        CartOffer cartOffer = new CartOffer();
        cartOffer.setEstablishmentId(productListing.getEstablishmentId());
        cartOffer.setOriginalPrice(productListing.getOriginalPrice());
        cartOffer.setDescription(productListing.getDescription());
        cartOffer.setName(productListing.getName());
        cartOffer.setId(productListing.getId());
        cartOffer.setProductId(productListing.getProductId());
        cartOffer.setEndsAt(productListing.getEndsAt());
        cartOffer.setRemQty(productListing.getRemQty());
        cartOffer.setSalePrice(productListing.getSalePrice());
        cartOffer.setStartsAt(productListing.getStartsAt());
        cartOffer.setTotalQty(productListing.getTotalQty());

        CartEstablishment establishment = new CartEstablishment();
        establishment.setAddress(storeListings.getAddress());
        establishment.setName(storeListings.getName());

        CartGeoLocation geoLocation = new CartGeoLocation();
        List<Double> coordinates = new ArrayList<>();
        coordinates.add(storeListings.getGeoLocation().getCoordinates().get(1));
        coordinates.add(storeListings.getGeoLocation().getCoordinates().get(0));
        geoLocation.setCoordinates(coordinates);
        establishment.setGeoLocation(geoLocation);
        cartOffer.setEstablishment(establishment);

        MyCartList myCart = new MyCartList(null);
        myCart.setOffer(cartOffer);
        myCart.setProduct(addToCart);
        myCart.setProductId(productListing.getProductId());
        myCart.setOfferId(productListing.getId());


        if (!isReplace) {
            myCart.setQuantity(Integer.valueOf(lqDetailTvCounter.getText().toString()));
            cartLists.add(myCart);
        } else {
            myCart.setQuantity(Integer.valueOf(lqDetailTvCounter.getText().toString()));
//            myCart.setQuantity(cartLists.get(index).getQuantity() + Integer.valueOf(lqDetailTvCounter.getText().toString()));
            cartLists.set(index, myCart);
        }
    }

    void addNewItemTocart() {
        CartProduct addToCart = productListing.getProduct();

        CartOffer cartOffer = new CartOffer();
        cartOffer.setEstablishmentId(productListing.getEstablishmentId());
        cartOffer.setOriginalPrice(productListing.getOriginalPrice());
        cartOffer.setDescription(productListing.getDescription());
        cartOffer.setName(productListing.getName());
        cartOffer.setId(productListing.getId());
        cartOffer.setProductId(productListing.getProductId());
        cartOffer.setEndsAt(productListing.getEndsAt());
        cartOffer.setRemQty(productListing.getRemQty());
        cartOffer.setSalePrice(productListing.getSalePrice());
        cartOffer.setStartsAt(productListing.getStartsAt());
        cartOffer.setTotalQty(productListing.getTotalQty());

        CartEstablishment establishment = new CartEstablishment();
        establishment.setAddress(storeListings.getAddress());
        establishment.setName(storeListings.getName());

        CartGeoLocation geoLocation = new CartGeoLocation();
        List<Double> coordinates = new ArrayList<>();
        coordinates.add(storeListings.getGeoLocation().getCoordinates().get(1));
        coordinates.add(storeListings.getGeoLocation().getCoordinates().get(0));
        geoLocation.setCoordinates(coordinates);
        establishment.setGeoLocation(geoLocation);

        cartOffer.setEstablishment(establishment);

        MyCartList myCart = new MyCartList(null);
        myCart.setOffer(cartOffer);
        myCart.setProduct(addToCart);
        myCart.setQuantity(Integer.valueOf(lqDetailTvCounter.getText().toString()));
        myCart.setProductId(productListing.getProductId());
        myCart.setOfferId(productListing.getId());
        cartLists.add(myCart);

    }


    void showEmptyCartDialog(final boolean isLogin) {


        emptyCartDialog = new EmptyCartDialog(mContext, new EmptyCartDialog.DialogClickListner() {
            @Override
            public void onClickNotify() {
                if (!isLogin) {
                    emptyCartDialog.dismiss();
                    cartLists = new ArrayList<>();
                    addNewItemTocart();
                    saveCartToPref(cartLists);
                } else {
                    emptyCartDialog.dismiss();

                    createRequestForEmptyCart();

                }
            }

            @Override
            public void onClickNotNow() {

                emptyCartDialog.dismiss();

            }
        });
        emptyCartDialog.setCanceledOnTouchOutside(false);
        emptyCartDialog.setCancelable(false);
        emptyCartDialog.show();

    }

    private void createRequestForEmptyCart() {

        LoginResponse loginResponse = preferenceManager.getLoginData();

        JsonObject object = new JsonObject();
        object.addProperty("accountId", loginResponse.getUserId());
        object.addProperty("state", 0);

        JsonObject state = new JsonObject();
        state.addProperty("state", 4);
        Utils.progressDialog(mContext);

        Call<ResponseBody> call = ApiModule.apiService()
                .createRequestForEmptyCart(object.toString(), state);
        call.enqueue(new RetrofitCallback<ResponseBody>(mContext, toolbar, new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response.isSuccessful())
                    addItemToCart();

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                Utils.progressDialogDismiss();
                System.out.println("LiquorDetailFragment.onFailure : " + t.getMessage());

            }
        }));
    }


    void showPreviewImage(String imgUrl, String title) {

        final Dialog openDialog = new Dialog(getContext(), R.style.Theme_AppCompat_Dialog_Alert);
        openDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        openDialog.setContentView(R.layout.preview_img);

        Toolbar mToolbar = openDialog.findViewById(R.id.prev_toolbar);
        mToolbar.setBackgroundColor(Color.TRANSPARENT);
        TextView mToolbarTitleTxt = openDialog.findViewById(R.id.toolbar_title);
        ImageView ivPreview = openDialog.findViewById(R.id.iv_preview_image);

        Glide.with(mContext).load(IMAGE_BASE_URL + imgUrl)/*.centerCrop()*/.placeholder(R.drawable.image_loading).error(R.drawable.no_image).dontAnimate().into(ivPreview);

        mToolbarTitleTxt.setText(title);

        mToolbar.inflateMenu(R.menu.close);
        mToolbar.setContentInsetStartWithNavigation(0);


        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog.dismiss();
            }
        });

        mToolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();
                switch (id) {


                    case R.id.action_close:


                        openDialog.dismiss();

                        break;

                }

                return true;
            }
        });


        openDialog.show();


    }

    private void setToolbar(final String title) {
//        Tooltitle = title;
        toolbarTitle.setText(title);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((MainActivity) getContext()).replaceFragment(getFragmentManager(), R.id.frame, ShopDetailFragment.newInstance(storeListings, productListing.getEstablishmentId(), Tooltitle), Const.ShopDetailFragment);


            }
        });


    }

    @OnClick({R.id.lq_detail_ivMinus, R.id.lq_detail_ivAdd})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.lq_detail_ivMinus:
                if (itemCount > 1) {
                    itemCount = itemCount - 1;
                    if (tvAddtoCart.getText().toString().equalsIgnoreCase("UPDATE CART")) {
                        tvAddtoCart.setClickable(true);
                        tvAddtoCart.setEnabled(true);
                        tvAddtoCart.setText("UPDATE CART");
                        lqDetailLlCart.setBackgroundResource(R.drawable.bg_button_login);
                        lqDetailTvCounter.setBackgroundResource(R.drawable.bg_button_login_dark);
                    }
                }
                lqDetailTvCounter.setText(String.valueOf(itemCount));
                break;
            case R.id.lq_detail_ivAdd:
                if (itemCount < productListing.getRemQty()) {
                    if (tvAddtoCart.getText().toString().equalsIgnoreCase("UPDATE CART")) {
                        tvAddtoCart.setClickable(true);
                        tvAddtoCart.setEnabled(true);
                        tvAddtoCart.setText("UPDATE CART");
                        lqDetailLlCart.setBackgroundResource(R.drawable.bg_button_login);
                        lqDetailTvCounter.setBackgroundResource(R.drawable.bg_button_login_dark);
                    }
                    itemCount = itemCount + 1;
                    lqDetailTvCounter.setText(String.valueOf(itemCount));
                } else {
                    new AlertDialog.Builder(mContext).setMessage("You reached max limit").setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).show();
                }
                break;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getData(String data) {
        if (data.equalsIgnoreCase("ConnectException")) {
            System.out.println("HomeFragment.getValueFromEventBus : ConnectException");
            cl_error.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.btnRetry)
    public void onRetryClick() {
        cl_error.setVisibility(View.GONE);
        EventBus.getDefault().unregister(this);
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.detach(LiquorDetailFragment.this).attach(LiquorDetailFragment.this).commitAllowingStateLoss();
    }
}
