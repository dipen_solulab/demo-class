package com.app.bevvi.model.cartModel;


import android.support.annotation.Nullable;

import com.app.bevvi.model.responseModel.OperatesAt;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CartEstablishment implements Serializable {

    @SerializedName("name")
    @Nullable
    @Expose
    private String name;
    @SerializedName("url")
    @Nullable
    @Expose
    private String url;
    @SerializedName("rating")
    @Nullable
    @Expose
    private Double rating;
    @SerializedName("operatesAt")
    @Nullable
    @Expose
    private OperatesAt operatesAt;
    @SerializedName("popularAt")
    @Nullable
    @Expose
    private CartPopularAt popularAt;
    @SerializedName("accountId")
    @Nullable
    @Expose
    private String accountId;
    @SerializedName("locationId")
    @Nullable
    @Expose
    private String locationId;
    @SerializedName("visits")
    @Nullable
    @Expose
    private CartVisits visits;
    @SerializedName("geoLocation")
    @Nullable
    @Expose
    private CartGeoLocation geoLocation;
    @SerializedName("menu")
    @Nullable
    @Expose
    private CartMenu menu;
    @SerializedName("address")
    @Nullable
    @Expose
    private String address;
    @SerializedName("hcRating")
    @Nullable
    @Expose
    private Integer hcRating;
    @SerializedName("deleted")
    @Nullable
    @Expose
    private Boolean deleted;
    @SerializedName("id")
    @Nullable
    @Expose
    private String id;
    @SerializedName("createdAt")
    @Nullable
    @Expose
    private String createdAt;
    @SerializedName("updatedAt")
    @Nullable
    @Expose
    private String updatedAt;
    @SerializedName("sourceId")
    @Nullable
    @Expose
    private String sourceId;
    @SerializedName("source")
    @Nullable
    @Expose
    private String source;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public OperatesAt getOperatesAt() {
        return operatesAt;
    }

    public void setOperatesAt(OperatesAt operatesAt) {
        this.operatesAt = operatesAt;
    }

    public CartPopularAt getPopularAt() {
        return popularAt;
    }

    public void setPopularAt(CartPopularAt popularAt) {
        this.popularAt = popularAt;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public CartVisits getVisits() {
        return visits;
    }

    public void setVisits(CartVisits visits) {
        this.visits = visits;
    }

    public CartGeoLocation getGeoLocation() {
        return geoLocation;
    }

    public void setGeoLocation(CartGeoLocation geoLocation) {
        this.geoLocation = geoLocation;
    }

    public CartMenu getMenu() {
        return menu;
    }

    public void setMenu(CartMenu menu) {
        this.menu = menu;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getHcRating() {
        return hcRating;
    }

    public void setHcRating(Integer hcRating) {
        this.hcRating = hcRating;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getSourceId() {
        return sourceId;
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

}