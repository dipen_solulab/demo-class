package com.app.bevvi.fragment;


import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.app.bevvi.R;
import com.app.bevvi.activity.LoginActivity;
import com.app.bevvi.activity.MainActivity;
import com.app.bevvi.activity.OrderDetailActivity;
import com.app.bevvi.activity.SettingsActivity;
import com.app.bevvi.adapter.PastOrderAdapter;
import com.app.bevvi.adapter.PendingOrderAdapter;
import com.app.bevvi.api.ApiModule;
import com.app.bevvi.api.RetrofitCallback;
import com.app.bevvi.model.responseModel.AccountDetail;
import com.app.bevvi.model.responseModel.GetPoints;
import com.app.bevvi.model.responseModel.LoginResponse;
import com.app.bevvi.model.responseModel.PendingOrderList;
import com.app.bevvi.utils.Const;
import com.app.bevvi.utils.PreferenceManager;
import com.app.bevvi.utils.Utils;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.DecimalFormat;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.CLIPBOARD_SERVICE;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {


    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.profile_ivClose)
    ImageView profileIvClose;
    @BindView(R.id.cart_frmPromo)
    FrameLayout cartFrmPromo;
    @BindView(R.id.profile_cl_Promo)
    ConstraintLayout profileClPromo;
    @BindView(R.id.profile_rvPending)
    RecyclerView profileRvPending;
    @BindView(R.id.profile_rvOrderComplete)
    RecyclerView profileRvOrderComplete;
    @BindView(R.id.edtPromoCode)
    TextView edtPromoCode;
    @BindView(R.id.tvCopyPromo)
    TextView tvCopyPromo;
    @BindView(R.id.tvPastTitle)
    TextView tvPastTitle;
    @BindView(R.id.tvPendingTitle)
    TextView tvPendingTitle;
    @BindView(R.id.tvPendingPickupTitle)
    TextView tvPendingPickupTitle;
    @BindView(R.id.profile_rvPendingPickup)
    RecyclerView profileRvPendingPickup;
    @BindView(R.id.tvRejectedTitle)
    TextView tvRejectedTitle;
    @BindView(R.id.profile_rvRejected)
    RecyclerView profileRvRejected;
    @BindView(R.id.tvCancelledTitle)
    TextView tvCancelledTitle;
    @BindView(R.id.profile_rvCancelled)
    RecyclerView profileRvCancelled;
    @BindView(R.id.tvAgeTitle)
    TextView tvAgeTitle;
    @BindView(R.id.profile_rvCancelledAge)
    RecyclerView profileRvCancelledAge;
    @BindView(R.id.pbUserPoints)
    ProgressBar pbUserPoints;
    @BindView(R.id.cl_error)
    ConstraintLayout cl_error;
    @BindView(R.id.profile_content)
    LinearLayout profile_content;
    @BindView(R.id.tvErnedPoints)
    TextView tvErnedPoints;
    @BindView(R.id.tvRemainingPoints)
    TextView tvRemainingPoints;
    @BindView(R.id.OrderPb)
    ProgressBar OrderPb;


    Unbinder unbinder;

    PendingOrderAdapter pendingPickupOrderAdapter;
    PendingOrderAdapter rejectedOrderAdapter;
    PendingOrderAdapter cancelledOrderAdapter;
    PendingOrderAdapter pendingOrderAdapter;
    PendingOrderAdapter cancelledAgeOrderAdapter;
    PastOrderAdapter pastOrderAdapter;

    ClipboardManager myClipboard;
    ClipData myClip;

    PreferenceManager preferenceManager;
    Context mContext;

    View rootView;

    ArrayList<PendingOrderList> pendingList = new ArrayList<>();
    ArrayList<PendingOrderList> pendingPickUPList = new ArrayList<>();
    ArrayList<PendingOrderList> pastList = new ArrayList<>();
    ArrayList<PendingOrderList> rejectedList = new ArrayList<>();
    ArrayList<PendingOrderList> cancelledList = new ArrayList<>();
    ArrayList<PendingOrderList> cancelledAgeList = new ArrayList<>();

    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public void onStart() {
        super.onStart();
        try {
            if (!EventBus.getDefault().isRegistered(this))
                EventBus.getDefault().register(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        // Inflate the layout for this fragment
        unbinder = ButterKnife.bind(this, rootView);
        preferenceManager = new PreferenceManager(mContext);
        myClipboard = (ClipboardManager) mContext.getSystemService(CLIPBOARD_SERVICE);
        setToolbar();

        if (preferenceManager.getBooleanPreference(PreferenceManager.ISLOGIN)) {
            AccountDetail accountDetail = preferenceManager.getAccountDetail(PreferenceManager.ACCOUNT_DETAIL);

            if (accountDetail != null) {

                edtPromoCode.setText(accountDetail.getCode());

            }

        }

        pendingOrderAdapter = new PendingOrderAdapter(mContext, preferenceManager, Const.ORDER_TYPE_PENDINGORDER, pendingList, new PendingOrderAdapter.MyClickListner() {
            @Override
            public void onItemClick(View view, int position) {

//                ((MainActivity) mContext).replaceFragment(getFragmentManager(), R.id.frame, new LiquorDetailFragment(), Const.LiquorDetailFragment);
                Intent intent = new Intent(mContext, OrderDetailActivity.class);
                intent.putExtra("isPastOrder", false);
                intent.putExtra("pendingList", pendingList.get(position));
                intent.putExtra("type", Const.ORDER_TYPE_PENDINGORDER);
                startActivity(intent);


            }


        });
        profileRvPending.setHasFixedSize(false);
        profileRvPending.setNestedScrollingEnabled(false);

        LinearLayoutManager rvInvitedLayoutManagaer = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        profileRvPending.setAdapter(pendingOrderAdapter);
        profileRvPending.setLayoutManager(rvInvitedLayoutManagaer);

        cancelledAgeOrderAdapter = new PendingOrderAdapter(mContext, preferenceManager, Const.ORDER_TYPE_CANCELLEDAGEORDER, cancelledAgeList, new PendingOrderAdapter.MyClickListner() {
            @Override
            public void onItemClick(View view, int position) {

//                ((MainActivity) mContext).replaceFragment(getFragmentManager(), R.id.frame, new LiquorDetailFragment(), Const.LiquorDetailFragment);
                Intent intent = new Intent(mContext, OrderDetailActivity.class);
                intent.putExtra("isPastOrder", false);
                intent.putExtra("pendingList", cancelledAgeList.get(position));
                intent.putExtra("type", Const.ORDER_TYPE_CANCELLEDAGEORDER);
                startActivity(intent);


            }


        });
        profileRvCancelledAge.setHasFixedSize(false);
        profileRvCancelledAge.setNestedScrollingEnabled(false);

        LinearLayoutManager rvprofileRvCancelledAge = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        profileRvCancelledAge.setAdapter(cancelledAgeOrderAdapter);
        profileRvCancelledAge.setLayoutManager(rvprofileRvCancelledAge);

        pastOrderAdapter = new PastOrderAdapter(mContext, preferenceManager, pastList, new PastOrderAdapter.MyClickListner() {
            @Override
            public void onItemClick(View view, int position) {

//                ((MainActivity) mContext).replaceFragment(getFragmentManager(), R.id.frame, new LiquorDetailFragment(), Const.LiquorDetailFragment);
                Intent intent = new Intent(mContext, OrderDetailActivity.class);
                intent.putExtra("isPastOrder", true);
                intent.putExtra("pendingList", pastList.get(position));
                intent.putExtra("type", Const.ORDER_TYPE_PASTORDER);
                startActivity(intent);


            }


        });
        profileRvOrderComplete.setHasFixedSize(false);
        profileRvOrderComplete.setNestedScrollingEnabled(false);

        LinearLayoutManager rvInvitedLayoutManagaer1 = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        profileRvOrderComplete.setAdapter(pastOrderAdapter);
        profileRvOrderComplete.setLayoutManager(rvInvitedLayoutManagaer1);

       /* PendingOrderAdapter pendingPickupOrderAdapter;
        PendingOrderAdapter rejectedOrderAdapter;
        PendingOrderAdapter cancelledOrderAdapter;*/
        pendingPickupOrderAdapter = new PendingOrderAdapter(mContext, preferenceManager, Const.ORDER_TYPE_PENDINGPICKUPORDER, pendingPickUPList, new PendingOrderAdapter.MyClickListner() {
            @Override
            public void onItemClick(View view, int position) {

//                ((MainActivity) mContext).replaceFragment(getFragmentManager(), R.id.frame, new LiquorDetailFragment(), Const.LiquorDetailFragment);
                Intent intent = new Intent(mContext, OrderDetailActivity.class);
                intent.putExtra("isPastOrder", false);
                intent.putExtra("pendingList", pendingPickUPList.get(position));
                intent.putExtra("type", Const.ORDER_TYPE_PENDINGPICKUPORDER);
                startActivity(intent);


            }


        });
        profileRvPendingPickup.setHasFixedSize(false);
        profileRvPendingPickup.setNestedScrollingEnabled(false);

        LinearLayoutManager rvPendingPickUpManagaer = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        profileRvPendingPickup.setAdapter(pendingPickupOrderAdapter);
        profileRvPendingPickup.setLayoutManager(rvPendingPickUpManagaer);


        rejectedOrderAdapter = new PendingOrderAdapter(mContext, preferenceManager, Const.ORDER_TYPE_REJECTEDORDER, rejectedList, new PendingOrderAdapter.MyClickListner() {
            @Override
            public void onItemClick(View view, int position) {

//                ((MainActivity) mContext).replaceFragment(getFragmentManager(), R.id.frame, new LiquorDetailFragment(), Const.LiquorDetailFragment);
                Intent intent = new Intent(mContext, OrderDetailActivity.class);
                intent.putExtra("isPastOrder", false);
                intent.putExtra("pendingList", rejectedList.get(position));
                intent.putExtra("type", Const.ORDER_TYPE_REJECTEDORDER);
                startActivity(intent);


            }


        });
        profileRvRejected.setHasFixedSize(false);
        profileRvRejected.setNestedScrollingEnabled(false);
        LinearLayoutManager rvRejectedManagaer = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        profileRvRejected.setAdapter(rejectedOrderAdapter);
        profileRvRejected.setLayoutManager(rvRejectedManagaer);

        cancelledOrderAdapter = new PendingOrderAdapter(mContext, preferenceManager, Const.ORDER_TYPE_CANCELLEDORDER, cancelledList, new PendingOrderAdapter.MyClickListner() {
            @Override
            public void onItemClick(View view, int position) {

//                ((MainActivity) mContext).replaceFragment(getFragmentManager(), R.id.frame, new LiquorDetailFragment(), Const.LiquorDetailFragment);
                Intent intent = new Intent(mContext, OrderDetailActivity.class);
                intent.putExtra("isPastOrder", false);
                intent.putExtra("pendingList", cancelledList.get(position));
                intent.putExtra("type", Const.ORDER_TYPE_CANCELLEDORDER);
                startActivity(intent);


            }


        });
        profileRvCancelled.setHasFixedSize(false);
        profileRvCancelled.setNestedScrollingEnabled(false);
        LinearLayoutManager rvCancelledManagaer = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        profileRvCancelled.setAdapter(cancelledOrderAdapter);
        profileRvCancelled.setLayoutManager(rvCancelledManagaer);


        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Utils.progressDialog(mContext);
        getPoints();

    }

    @Override
    public void onResume() {
        super.onResume();
        Utils.progressDialogDismiss();
        Utils.progressDialog(mContext);
        getOrderData(0); //PendingAcceptance
    }

    //This function will get fire when user came from order summrary to refresh page
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getData(String data) {
        System.out.println("ProfileFragment.getData : " + data);
        if (data.equalsIgnoreCase("doProfilePageRefresh")) {
            EventBus.getDefault().unregister(this);
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.detach(ProfileFragment.this).attach(ProfileFragment.this).commitAllowingStateLoss();

        } else if (data.equalsIgnoreCase("ConnectException")) {
            System.out.println("HomeFragment.getValueFromEventBus : ConnectException");
            Toast.makeText(mContext, "ConnectException", Toast.LENGTH_SHORT).show();
            cl_error.setVisibility(View.VISIBLE);
            profile_content.setVisibility(View.GONE);

        }
    }

    @OnClick(R.id.btnRetry)
    public void onRetryClick() {
        cl_error.setVisibility(View.GONE);
        profile_content.setVisibility(View.VISIBLE);
        EventBus.getDefault().unregister(this);
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.detach(ProfileFragment.this).attach(ProfileFragment.this).commitAllowingStateLoss();
    }

    private void getPoints() {
        Utils.progressDialog(mContext);
        Call<GetPoints> call = ApiModule.apiService().getPoints(preferenceManager.getLoginData().getUserId(), preferenceManager.getLoginData().getAccessToken());
        call.enqueue(new Callback<GetPoints>() {

            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<GetPoints> call, Response<GetPoints> response) {

                if (response.isSuccessful()) {
                    if (Utils.isFragmentVisible(ProfileFragment.this)) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            pbUserPoints.setProgress(response.body().getNetPoints(), true);
                        } else
                            pbUserPoints.setProgress(response.body().getNetPoints());

                        tvErnedPoints.setText("You have a total of " + response.body().getNetPoints() + " points");
                        DecimalFormat formatter = new DecimalFormat("#,###,###");
                        String amountString = formatter.format(1000 - response.body().getNetPoints());
                        tvRemainingPoints.setText("You are " + amountString + " points away from a $10 reward!");
                    }

                } else if (response.raw().code() == 401) {
                    ((AppCompatActivity) mContext).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (!((AppCompatActivity) mContext).isFinishing()) {

                                new AlertDialog.Builder(mContext).setTitle("Alert").setMessage(R.string.session_expire).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        PreferenceManager preferenceManager = new PreferenceManager(mContext);
                                        preferenceManager.clearPrefrences();
                                        preferenceManager.setStringPreference(PreferenceManager.FCM_TOKEN, FirebaseInstanceId.getInstance().getToken());
                                        Intent intent = new Intent(mContext, LoginActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        ((AppCompatActivity) mContext).finish();
                                        startActivity(intent);
                                        ((AppCompatActivity) mContext).finishAffinity();

                                    }
                                }).setCancelable(false).show();

                            }
                        }
                    });

                }
                Utils.progressDialogDismiss();
            }

            @Override
            public void onFailure(Call<GetPoints> call, Throwable t) {
                System.out.println("HomeFragment.onFailure : " + t.getMessage());
                Utils.progressDialogDismiss();

            }
        });


    }

    private void getOrderData(final int status) {
//        {"where":{"accountId":"123412341234","status":0},"include":["establishment","transactions",{"orderdetails":["product"]}]}
        System.out.println("ProfileFragment.getOrderData status : " + status);
        LoginResponse loginResponse = preferenceManager.getLoginData();

        JsonObject object = new JsonObject();

        JsonObject where = new JsonObject();
        where.addProperty("accountId", loginResponse.getUserId());
//        where.addProperty("accountId", "5a4f7661a83c0626326c0e7b");
        where.addProperty("status", status);

        JsonArray include = new JsonArray();
        include.add("establishment");
        include.add("transactions");

        JsonArray orderdetails = new JsonArray();
        orderdetails.add("product");


        JsonObject order = new JsonObject();
        order.add("orderdetails", orderdetails);

        include.add(order);

        object.add("where", where);
        object.add("include", include);
        object.addProperty("order", "pickupTime DESC");


        System.out.println("ProfileFragment.getPendingData : " + object.toString());

        Call<ArrayList<PendingOrderList>> call = ApiModule.apiService().getPastPendingOrder(object.toString(), loginResponse.getAccessToken());
        call.enqueue(new RetrofitCallback<ArrayList<PendingOrderList>>(mContext, toolbar, new Callback<ArrayList<PendingOrderList>>() {

            @Override
            public void onResponse(Call<ArrayList<PendingOrderList>> call, Response<ArrayList<PendingOrderList>> response) {


                if (response.isSuccessful()) {
                    int mStatus = status;
                    if (Utils.isFragmentVisible(ProfileFragment.this)) {
                        setData(response, mStatus);

                        if (mStatus < 5) {
                            mStatus = mStatus + 1;
                            System.out.println("getOrderData.onResponse mStatus: " + mStatus);
                            getOrderData(mStatus);
                        } else {
                            Utils.progressDialogDismiss();
                            OrderPb.setVisibility(View.GONE);
                        }
                    } else
                        Utils.progressDialogDismiss();

                } else if (response.raw().code() == 401) {
                    OrderPb.setVisibility(View.GONE);
                    Utils.progressDialogDismiss();
                    ((AppCompatActivity) mContext).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (!((AppCompatActivity) mContext).isFinishing()) {

                                new AlertDialog.Builder(mContext).setTitle("Alert").setMessage(R.string.session_expire).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        PreferenceManager preferenceManager = new PreferenceManager(mContext);
                                        preferenceManager.clearPrefrences();
                                        preferenceManager.setStringPreference(PreferenceManager.FCM_TOKEN, FirebaseInstanceId.getInstance().getToken());
                                        Intent intent = new Intent(mContext, LoginActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        ((AppCompatActivity) mContext).finish();
                                        startActivity(intent);
                                        ((AppCompatActivity) mContext).finishAffinity();

                                    }
                                }).setCancelable(false).show();

                            }
                        }
                    });

                }

            }

            @Override
            public void onFailure(Call<ArrayList<PendingOrderList>> call, Throwable t) {
                System.out.println("deleteAllAccessToken.onFailure : " + t.getMessage());
                OrderPb.setVisibility(View.GONE);
                Utils.progressDialogDismiss();
            }
        }));


    }

    private void setData(Response<ArrayList<PendingOrderList>> response, int status) {
        System.out.println("ProfileFragment.setData status : " + status);
        if (status == 0) {
            if (response.body().isEmpty()) {

                tvPendingTitle.setVisibility(View.GONE);
            } else {
                System.out.println("ProfileFragment.setData : pendingList");
                tvPendingTitle.setVisibility(View.VISIBLE);
                pendingList.clear();
                pendingList.addAll(response.body());
                pendingOrderAdapter.notifyDataSetChanged();

            }
        } else if (status == 1) {

            int viewCount = 0;
            for (int i = 0; i < response.body().size(); i++) {
                if (!response.body().get(i).isViewed())
                    viewCount = viewCount + 1;
            }
            if (viewCount > 0) {
                ((MainActivity) mContext).profileItemView.removeView(((MainActivity) mContext).pendingBadge);
                ((MainActivity) mContext).profileItemView.addView(((MainActivity) mContext).pendingBadge);
            } else {
                ((MainActivity) mContext).profileItemView.removeView(((MainActivity) mContext).pendingBadge);
            }

            if (response.body().isEmpty()) {
                tvPendingPickupTitle.setVisibility(View.GONE);
//                ((MainActivity) mContext).profileItemView.removeView(((MainActivity) mContext).pendingBadge);
            } else {
//                ((MainActivity) mContext).profileItemView.removeView(((MainActivity) mContext).pendingBadge);
//                ((MainActivity) mContext).profileItemView.addView(((MainActivity) mContext).pendingBadge);
                System.out.println("ProfileFragment.setData : pastList");
                tvPendingPickupTitle.setVisibility(View.VISIBLE);
                pendingPickUPList.clear();
                pendingPickUPList.addAll(response.body());
                pendingPickupOrderAdapter.notifyDataSetChanged();
            }
        } else if (status == 2) {
            if (response.body().isEmpty()) {
                tvPastTitle.setVisibility(View.GONE);
            } else {
                System.out.println("ProfileFragment.setData : pastList");
                tvPastTitle.setVisibility(View.VISIBLE);
                pastList.clear();
                pastList.addAll(response.body());
                pastOrderAdapter.notifyDataSetChanged();
            }
        } else if (status == 3) {
            if (response.body().isEmpty()) {
                tvRejectedTitle.setVisibility(View.GONE);
            } else {
                System.out.println("ProfileFragment.setData : rejectedList");
                tvRejectedTitle.setVisibility(View.VISIBLE);
                rejectedList.clear();
                rejectedList.addAll(response.body());
                rejectedOrderAdapter.notifyDataSetChanged();
            }
        } else if (status == 4) {
            if (response.body().isEmpty()) {
                tvCancelledTitle.setVisibility(View.GONE);
            } else {
                System.out.println("ProfileFragment.setData : rejectedList");
                tvCancelledTitle.setVisibility(View.VISIBLE);
                cancelledList.clear();
                cancelledList.addAll(response.body());
                cancelledOrderAdapter.notifyDataSetChanged();
            }
        } else if (status == 5) {
            if (response.body().isEmpty()) {
                tvAgeTitle.setVisibility(View.GONE);
            } else {
                System.out.println("ProfileFragment.setData : rejectedList");
                tvAgeTitle.setVisibility(View.VISIBLE);
                cancelledAgeList.clear();
                cancelledAgeList.addAll(response.body());
                cancelledAgeOrderAdapter.notifyDataSetChanged();
            }
        }


    }


    private void setToolbar() {
        toolbar.inflateMenu(R.menu.settigns);
        toolbarTitle.setText("Profile");

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();
                switch (id) {

                    case R.id.action_settting:

                        Intent intent = new Intent(mContext, SettingsActivity.class);
                        startActivity(intent);

                        break;

                }

                return true;
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.profile_ivClose)
    public void onViewClicked() {

        profileClPromo.setVisibility(View.GONE);

    }

    @OnClick({R.id.tvCopyPromo, R.id.btnSendInvite})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tvCopyPromo:

                String text = edtPromoCode.getText().toString();
                myClip = ClipData.newPlainText("text", text);
                myClipboard.setPrimaryClip(myClip);
                Toast.makeText(mContext, "Text Copied", Toast.LENGTH_SHORT).show();

                break;
            case R.id.btnSendInvite:
                AccountDetail accountDetail = preferenceManager.getAccountDetail(PreferenceManager.ACCOUNT_DETAIL);

                if (accountDetail != null) {


                    String message = "Get your drink on with Bevvi. " +
                            "We got you covered on the greatest deals, even your mother will be happy." +
                            " Here is $5 off your first order with promo code :"
                            + accountDetail.getCode() + "\n\nOrder now: https://bevvi.app.link/playstore";
                    Utils.shareApp(mContext, message);


                }

                break;
        }
    }
}
