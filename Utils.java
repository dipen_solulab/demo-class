package com.app.bevvi.utils;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.customtabs.CustomTabsIntent;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.bevvi.BuildConfig;
import com.app.bevvi.R;
import com.app.bevvi.model.responseModel.ErrorModel;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import static android.content.Context.INPUT_METHOD_SERVICE;

/**
 * Created by Nirmal on 12-05-16.
 */
public class Utils {

    /**
     * The constant TAG.
     */
//TAG
    public static String TAG = Utils.class.getSimpleName();

    /**
     * The constant days.
     */
    public static ArrayList<String> days = new ArrayList<>();
    /**
     * The constant progressDialog.
     */
//
    static ProgressDialog progressDialog;

    /**
     * The Perm array.
     */
    public static String[] perm_array = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };

    /**
     * Make toast.
     *
     * @param context the context
     * @param message the message
     */
    public static void makeToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    /**
     * Is network available boolean.
     *
     * @param mContext the m context
     * @return the boolean
     */
    public static boolean isNetworkAvailable(Context mContext) {

        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm != null) {
            NetworkInfo[] info = cm.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        Log.e("Network Testing", "***Available***");
                        return true;
                    }
        }
        return false;
    }

    /**
     * Split string string [ ].
     *
     * @param string   the string
     * @param seprator the seprator
     * @return the string [ ]
     */
    public static String[] splitString(String string, String seprator) {

        String[] items = string.split(seprator);

        return items;
    }

    /**
     * Sets badge count.
     *
     * @param context the context
     * @param icon    the icon
     * @param count   the count
     */
    public static void setBadgeCount(Context context, LayerDrawable icon, String count) {

        BadgeDrawable badge;

        // Reuse drawable if possible
        Drawable reuse = icon.findDrawableByLayerId(R.id.ic_badge);
        if (reuse != null && reuse instanceof BadgeDrawable) {
            badge = (BadgeDrawable) reuse;
        } else {
            badge = new BadgeDrawable(context);
        }

        badge.setCount(count);
        icon.mutate();
        icon.setDrawableByLayerId(R.id.ic_badge, badge);
    }

    /**
     * Formate date fromstring string.
     *
     * @param inputFormat  the input format
     * @param outputFormat the output format
     * @param inputDate    the input date
     * @return the string
     */
    public static String formateDateFromstring(String inputFormat, String outputFormat, String inputDate) {

        Date parsed = null;
        String outputDate = "";

        SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, Locale.getDefault());
        SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, Locale.getDefault());

        try {
            parsed = df_input.parse(inputDate);
            outputDate = df_output.format(parsed);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return outputDate;

    }

    /**
     * Count spaces int.
     *
     * @param string the string
     * @return the int
     */
    public static int countSpaces(String string) {
        int spaces = 0;
        for (int i = 0; i < string.length(); i++) {
            spaces += (Character.isWhitespace(string.charAt(i))) ? 1 : 0;
        }
        return spaces;
    }

    /**
     * Gets utc from date time.
     *
     * @param pattern        the pattern
     * @param dateTimeString the date time string
     * @return the utc from date time
     */
    public static String getUTCFromDateTime(String pattern, String dateTimeString) {
        String dateString = "";
        try {
            long dateInMillis = Info.dateFormat_new.parse(dateTimeString).getTime();
            SimpleDateFormat formatter = new SimpleDateFormat(pattern, Locale.getDefault());
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
            dateString = formatter.format(new Date(dateInMillis));
            //System.out.println("finalUTCDate -> "+dateString);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dateString;
    }


    /**
     * Gets localfrom utc.
     *
     * @param utc_data      the utc data
     * @param inputPattern  the input pattern
     * @param outputPattern the output pattern
     * @return the localfrom utc
     */
    public static String getLocalfromUtc(String utc_data, String inputPattern, String outputPattern) {
        String formattedDate = "";
        try {
            SimpleDateFormat df = new SimpleDateFormat(inputPattern, Locale.getDefault());
            df.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date date = null;
            try {
                date = df.parse(utc_data);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            //System.out.println(utc_data);
            df.setTimeZone(TimeZone.getDefault());
            SimpleDateFormat df1 = new SimpleDateFormat(outputPattern, Locale.getDefault());
            formattedDate = df1.format(date);
            //System.out.println("finalLocalTimeDate -> "+formattedDate);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return formattedDate;
    }

    /**
     * Gets time from utc.
     *
     * @param givenDateString the given date string
     * @return the time from utc
     */
    public static String getTimeFromUTC(String givenDateString) {

        String dateStr = "";
        try {
            SimpleDateFormat df = new SimpleDateFormat("HH:mm");
            df.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date date = null;

            date = df.parse(givenDateString);


            df.setTimeZone(TimeZone.getDefault());
            dateStr = df.format(date);
            dateStr = Info.Hr24TimeFormat.format(date);


        } catch (ParseException e) {
            e.printStackTrace();
        }

        return dateStr;
    }

    /**
     * Gets time from date.
     *
     * @param date the date
     * @return the time from date
     */
    public static String getTimeFromDate(Date date) {
        SimpleDateFormat localDateFormat = new SimpleDateFormat("HH:mm");
        String time = localDateFormat.format(date);
        System.out.println(time);
        return time;
    }

    /**
     * Gets date time from date.
     *
     * @param date the date
     * @return the date time from date
     */
    public static String getDateTimeFromDate(Date date) {
//        2017-04-13 12:00
        SimpleDateFormat localDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm",Locale.getDefault());
        String time = localDateFormat.format(date);
        System.out.println(time);
        return time;
    }

    /**
     * Gets day.
     *
     * @param date the date
     * @return the day
     */
    public static int getDay(Date date) {
        Calendar cal = Calendar.getInstance(Locale.getDefault());
        cal.setTime(date);
        return cal.get(Calendar.DAY_OF_WEEK);
    }

    /**
     * Gets version name.
     *
     * @return the version name
     */
    public static String getVersionName() {
        StringBuilder builder = new StringBuilder();
        /*use below line for version number like : 4.4.1*/
//        builder.append(Build.VERSION.RELEASE);

        Field[] fields = Build.VERSION_CODES.class.getFields();
        for (Field field : fields) {
            String fieldName = field.getName();
            int fieldValue = -1;

            try {
                fieldValue = field.getInt(new Object());
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }

            if (fieldValue == Build.VERSION.SDK_INT) {
                builder.append(fieldName);
                /*user below line for sdk version like : 21*/
//                builder.append("sdk=").append(fieldValue);
            }
        }

        return builder.toString();

    }

    /**
     * Gets days diffrance.
     *
     * @param inputFormate the input formate
     * @param currentDate  the current date
     * @param endDate      the end date
     * @return the days diffrance
     */
    public static int getDaysDiffrance(String inputFormate, String currentDate, String endDate) {
        SimpleDateFormat myFormat = new SimpleDateFormat(inputFormate, Locale.getDefault());
        endDate = Utils.getLocalfromUtc(endDate, "yyyy-MM-dd'T'HH:mm:ss.SSS", "yyyy-MM-dd");
        System.out.println("Utils.getDaysDiffrance : " + endDate);
        int diffrance = 0;

        try {
            Date date1 = myFormat.parse(currentDate);
            Date date2 = myFormat.parse(endDate);
            long diff = date2.getTime() - date1.getTime();
            diffrance = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
            System.out.println("Days: " + diffrance);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return diffrance;

    }

    /**
     * Gets app version.
     *
     * @return the app version
     */
    public static String getAppVersion() {

        int versionCode = BuildConfig.VERSION_CODE;
        String versionName = BuildConfig.VERSION_NAME;

        return versionName;
    }

    /**
     * Is connected to internet boolean.
     *
     * @param context the context
     * @return the boolean
     */
    public static boolean isConnectedToInternet(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
        }
        return false;
    }

    /**
     * Validation Method for Edit Text
     *
     * @param editText          the edit text
     * @param emptyErrorMessage the empty error message
     * @return the boolean
     */
    public static boolean isEmptyString(EditText editText, String emptyErrorMessage) {
        if ((editText.getText().toString().trim()).length() == 0) {
            editText.setText("");
            editText.setError(emptyErrorMessage);
            editText.requestFocus();
            return true;
        } else return false;
    }

    /**
     * Is gps enabled boolean.
     *
     * @param mContext the m context
     * @return the boolean
     */
    public static boolean isGPSEnabled(Context mContext) {
        LocationManager manager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        return manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    /**
     * Compress img 1 string.
     *
     * @param filePath the file path
     * @return the string
     */
    public static String compressImg1(String filePath) {
        //System.out.println("File path : " + filePath);
        File file = new File(filePath);
        // System.out.println("File path name : " + file.getName());

        Bitmap bmp = BitmapFactory.decodeFile(filePath);
        FileOutputStream out = null;
        String filename = getFilename(file.getName());

        try {
            ExifInterface ei = new ExifInterface(filePath);
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            System.out.println("Orientation---<> " + orientation);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
//                    bitmap = rotateBitmap(bitmap, 90f);
                    bmp = rotateBitmap(bmp, 90f);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
//                    bitmap = rotateBitmap(bitmap, 180f);
                    bmp = rotateBitmap(bmp, 180f);
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            out = new FileOutputStream(filename);
//   write the compressed bitmap at the destination specified by filename.
            bmp.compress(Bitmap.CompressFormat.JPEG, 70, out);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return filePath;
        }

//        System.out.println("File path final name : " + filename);
        return filename;
    }

    /**
     * Compress img string.
     *
     * @param filePath the file path
     * @return the string
     */
    public static String compressImg(String filePath) {


//        Bitmap bitmap = null;
        Bitmap bmp = null;
/*
        try {
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
//            bitmap = BitmapFactory.decodeFile(filePath, bmOptions);
            bmp = BitmapFactory.decodeFile(filePath, bmOptions);
            ExifInterface ei = new ExifInterface(filePath);
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            System.out.println("Orientation---<> " + orientation);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
//                    bitmap = rotateBitmap(bitmap, 90f);
                    bmp = rotateBitmap(bmp, 90f);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
//                    bitmap = rotateBitmap(bitmap, 180f);
                    bmp = rotateBitmap(bmp, 180f);
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
*/

        System.out.println("File path : " + filePath);
        File file = new File(filePath);
        System.out.println("File path name : " + file.getName());


        FileOutputStream out = null;
        String filename = getFilename(file.getName());
        try {
            out = new FileOutputStream(filename);
//   write the compressed bitmap at the destination specified by filename.
            bmp.compress(Bitmap.CompressFormat.JPEG, 95, out);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return filePath;
        }
        System.out.println("File path final name : " + filename);
        return filename;
    }

    /**
     * Gets filename.
     *
     * @param filename the filename
     * @return the filename
     */
    public static String getFilename(String filename) {
        String fileName;
        File file1 = new File(filename);
        File file = new File(Environment.getExternalStorageDirectory(), "Jolly Motors/Jolly Motors Pictures/");
        if (!file.exists()) {
            file.mkdirs();
        }
        String filenameArray[] = file1.getName().split("\\.");
        String extension = filenameArray[filenameArray.length - 1];
        if (extension.equalsIgnoreCase("png") || extension.equalsIgnoreCase("jpeg")) {
            fileName = filenameArray[0] + ".jpg";
        } else {
            fileName = file1.getName();
        }
        String uriSting = (file.getAbsolutePath() + "/" + fileName);
        return uriSting;
    }

    /**
     * Call activity.
     *
     * @param mContext     the m context
     * @param activityName the activity name
     */
    public static void callActivity(Context mContext, Class activityName) {

        Intent intent = new Intent(mContext, activityName);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mContext.startActivity(intent);

    }

    /**
     * Is fragment visible boolean.
     *
     * @param fragment the fragment
     * @return the boolean
     */
    public static boolean isFragmentVisible(Fragment fragment) {

        if (fragment.isVisible()) {
            return true;
        } else
            return false;

    }

    /**
     * Make snakebare.
     *
     * @param view the view
     * @param msg  the msg
     */
    public static void makeSnakebare(View view, String msg) {

        Snackbar.make(view, msg, Snackbar.LENGTH_LONG).show();
    }

    /**
     * Handle error data.
     *
     * @param mContext the m context
     * @param response the response
     */
    public static void handleErrorData(Activity mContext, retrofit2.Response<?> response) {
        Gson gson = new GsonBuilder().create();
        ErrorModel errorModel = new ErrorModel();
        try {
            errorModel = gson.fromJson(response.errorBody().string(), ErrorModel.class);
            Toast.makeText(mContext, errorModel.getError().getDetails().getMessages().getEmail().get(0).toString(), Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            // handle failure to read error
            e.printStackTrace();
        }
    }

    /**
     * Convert dp to pixel int.
     *
     * @param dp      the dp
     * @param context the context
     * @return the int
     */
    public static int convertDpToPixel(int dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        int px = dp * ((int) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

    /**
     * Convert pixels to dp int.
     *
     * @param px      the px
     * @param context the context
     * @return the int
     */
    public static int convertPixelsToDp(int px, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        int dp = px / ((int) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return dp;
    }

    /**
     * Share app.
     *
     * @param mContext the m context
     * @param message  the message
     */
    public static void shareApp(Context mContext, String message) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, message);
        sendIntent.setType("text/plain");
        mContext.startActivity(Intent.createChooser(sendIntent, "Share to"));
    }

    /**
     * The interface Snake bar click.
     */
    public interface SnakeBarClick {
        /**
         * On item click.
         *
         * @param view the view
         */
        void onItemClick(View view);

    }

    /**
     * Make snakebar from top.
     *
     * @param context the context
     * @param toolbar the toolbar
     * @param v       the v
     * @param message the message
     */
    public static void makeSnakebarFromTop(Context context, Toolbar toolbar, View v, String message) {
        Snackbar snack = Snackbar.make(v, message, Snackbar.LENGTH_LONG);
        View view = snack.getView();
        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) view.getLayoutParams();
        params.gravity = Gravity.TOP;
        params.setMargins(0, toolbar.getHeight(), 0, 0);
        view.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
        view.setLayoutParams(params);
        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(ContextCompat.getColor(context, R.color.colorWhite));
        tv.setGravity(Gravity.CENTER);
        snack.show();
    }

    /**
     * Make snakebar.
     *
     * @param view          the view
     * @param msg           the msg
     * @param snakeBarClick the snake bar click
     */
    public static void makeSnakebar(View view, String msg, final SnakeBarClick snakeBarClick) {


        Snackbar.make(view, msg, Snackbar.LENGTH_INDEFINITE).setAction("RETRY", new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                snakeBarClick.onItemClick(view);

            }
        }).setActionTextColor(Color.RED).show();
    }

    /**
     * Sets location.
     *
     * @param lat     the lat
     * @param lng     the lng
     * @param context the context
     */
    public static void setLocation(Double lat, Double lng, Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor mEdit = sp.edit();
        mEdit.remove("userLat");
        mEdit.remove("userLng");
        mEdit.putString("userLat", String.valueOf(lat));
        mEdit.putString("userLng", String.valueOf(lng));
        mEdit.commit();
        return;
    }

    /**
     * Gets user location.
     *
     * @param context the context
     * @return the user location
     */
    public static Location getUserLocation(Context context) {
        SharedPreferences mSharedPreference = PreferenceManager.getDefaultSharedPreferences(context);
        Double lat = Double.valueOf(mSharedPreference.getString("userLat", "0.00"));
        Double lng = Double.valueOf(mSharedPreference.getString("userLng", "0.00"));
        Location user = new Location("user");
        user.setLatitude(lat);
        user.setLongitude(lng);
        return user;
    }

    /**
     * The constant placeIDList.
     */
    public static ArrayList placeIDList = null;

    /*public static ArrayList autocomplete(String input) {
        ArrayList resultList = null;


        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);
//            sb.append("&components=country:gr");
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));

            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
            resultList = new ArrayList(predsJsonArray.length());
            placeIDList = new ArrayList(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                System.out.println(predsJsonArray.getJSONObject(i).getString("place_id"));
                System.out.println("============================================================");
                resultList.add(predsJsonArray.getJSONObject(i).getString("description"));
                placeIDList.add(predsJsonArray.getJSONObject(i).getString("place_id"));
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Cannot process JSON results", e);
        }

        return resultList;
    }*/

    /**
     * Open app store.
     *
     * @param activity the activity
     */
    public static void openAppStore(Activity activity) {
        try {
            // "market://details?id=com.example.abc"
            String playStorePath = "market://details?id=" + activity.getPackageName();
            Uri uri = Uri.parse(playStorePath);
            Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
            activity.startActivity(myAppLinkToMarket);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets complete address string.
     *
     * @param context   the context
     * @param LATITUDE  the latitude
     * @param LONGITUDE the longitude
     * @return the complete address string
     */
    public static String getCompleteAddressString(Context context, double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                System.out.println("Utils.getCompleteAddressString : " + returnedAddress.getFeatureName());
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append(",");
                }
                strAdd = strReturnedAddress.toString();
                Log.w("My  loction address", "" + strReturnedAddress.toString());
            } else {
                Log.w("My  loction address", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.w("My  loction address", "Canont get Address!");
        }
        return strAdd;
    }

    /**
     * Hides the soft keyboard
     *
     * @param activity the activity
     */
    public static void hideSoftKeyboard(Activity activity) {
        if (activity.getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }
    }

    /**
     * Gets address from location.
     *
     * @param context     the context
     * @param m_latitude  the m latitude
     * @param m_longitude the m longitude
     * @return the address from location
     */
    public synchronized static Address getAddressFromLocation(Context context, double m_latitude, double m_longitude) {
        String my_city_name = "";
        Address address = null;
        try {
            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(m_latitude, m_longitude, 1);
            StringBuilder sb = new StringBuilder();
            if (addresses.size() > 0) {
                address = addresses.get(0);
//                sb.append(address.getLocality());
            }
//            my_city_name = sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return address;
    }

    /**
     * Gets custom date string.
     *
     * @param strdate the strdate
     * @return the custom date string
     */
    public static String getCustomDateString(String strdate) {

        String str = "";
        try {

            String strStartDateTime = Utils.getLocalfromUtc(strdate, "", "");

            str = Utils.formateDateFromstring("MMM dd, yyyy HH:mm", "MMM d", strStartDateTime);

       /*     Date oldDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(strdate);
            Date oldDate1 = Info.dbDateTimeFormat1.parse(strdate);
*/
            SimpleDateFormat tmp = new SimpleDateFormat("MMMM d");

//            Date date = tmp.parse(strdate);

//            str = tmp.format(oldDate);
            Date oldDate = tmp.parse(str);
            str = str.substring(0, 1).toUpperCase() + str.substring(1);

            if (oldDate.getDate() > 10 && oldDate.getDate() < 14)
                str = str + " th";
            else {
                if (str.endsWith("1")) str = str + " st";
                else if (str.endsWith("2")) str = str + " nd";
                else if (str.endsWith("3")) str = str + " rd";
                else str = str + " th";
            }

//        tmp = new SimpleDateFormat("yyyy");
//            str = str + tmp.format(oldDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    /**
     * Split by number string [ ].
     *
     * @param s    the s
     * @param size the size
     * @return the string [ ]
     */
    public static String[] splitByNumber(String s, int size) {
        if (s == null || size <= 0)
            return null;
        int chunks = s.length() / size + ((s.length() % size > 0) ? 1 : 0);
        String[] arr = new String[chunks];
        for (int i = 0, j = 0, l = s.length(); i < l; i += size, j++)
            arr[j] = s.substring(i, Math.min(l, i + size));
        return arr;
    }


    /**
     * Gets deviceid.
     *
     * @param context the context
     * @return the deviceid
     */
    public static String getDeviceid(Context context) {
        String android_id = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        return android_id;
    }

    /**
     * Is valid email boolean.
     *
     * @param target the target
     * @return the boolean
     */
    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    /**
     * Is valid mobile boolean.
     *
     * @param phone the phone
     * @return the boolean
     */
    public static boolean isValidMobile(String phone) {
        return android.util.Patterns.PHONE.matcher(phone).matches();
    }

    /**
     * Progress dialog.
     *
     * @param activtiy the activtiy
     */
    public static void progressDialog(Activity activtiy) {
        if (progressDialog != null)
            if (progressDialog.isShowing())
                progressDialog.dismiss();
        progressDialog = new ProgressDialog(activtiy, R.style.AppCompatAlertDialogStyle);

        progressDialog.setTitle(null);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();

    }

    /**
     * Progress dialog.
     *
     * @param context the context
     */
    public static void progressDialog(Context context) {
        if (progressDialog != null)
            if (progressDialog.isShowing())
                progressDialog.dismiss();
        progressDialog = new ProgressDialog(context, R.style.AppCompatAlertDialogStyle);

        progressDialog.setTitle(null);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();

    }


    /**
     * Progress dialog dismiss.
     */
    public static void progressDialogDismiss() {
        if (progressDialog != null)
            if (progressDialog.isShowing())
                progressDialog.dismiss();
    }

    /**
     * Open url.
     *
     * @param context the context
     * @param url     the url
     */
    public static void openUrl(Context context, String url) {
        CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
        builder.setToolbarColor(ContextCompat.getColor(context, R.color.colorPrimary));
        builder.setCloseButtonIcon(BitmapFactory.decodeResource(
                context.getResources(), R.drawable.ic_back));

        CustomTabsIntent customTabsIntent = builder.build();
        customTabsIntent.launchUrl(context, Uri.parse(url));
    }

    /**
     * Gets city name.
     *
     * @param context  the context
     * @param location the location
     * @return the city name
     */
    public static String getCityName(Context context, Location location) {
        String Cityname = "";
        Geocoder geoCoder = new Geocoder(context, Locale.getDefault());
        try {
            List<Address> address = geoCoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            int maxLines = address.get(0).getMaxAddressLineIndex();
            for (int i = 0; i < maxLines; i++) {
                Cityname = address.get(0).getLocality();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return Cityname;
    }

    /**
     * Make dialog with ok.
     *
     * @param mContext the m context
     * @param message  the message
     */
    public static void makeDialogWithOk(Context mContext, String message) {
        new AlertDialog.Builder(mContext).setMessage(message).setCancelable(false).setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

            }
        }).show();

    }

    /**
     * Change date formate string.
     *
     * @param time the time
     * @return the string
     */
    public static String ChangeDateFormate(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd MMM, yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    /**
     * Keyboard hide.
     *
     * @param context the context
     */
    public static void KeyboardHide(Context context) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
    }


    /**
     * Rotate image if needed bitmap.
     *
     * @param photoPath the photo path
     * @return the bitmap
     */
    public static Bitmap rotateImageIfNeeded(String photoPath) {
        Bitmap bitmap = null;
        try {
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            bitmap = BitmapFactory.decodeFile(photoPath, bmOptions);

            ExifInterface ei = new ExifInterface(photoPath);
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            System.out.println("Orientation---<> " + orientation);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    bitmap = rotateBitmap(bitmap, 90f);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    bitmap = rotateBitmap(bitmap, 180f);
                    break;
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    /**
     * Rotate bitmap bitmap.
     *
     * @param source the source
     * @param angle  the angle
     * @return the bitmap
     */
// Rotate given bitmap to desired angle like 90,180 degrees
    public static Bitmap rotateBitmap(Bitmap source, float angle) {
        System.out.println("Rotate--> " + angle);
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    /**
     * Create file from url file.
     *
     * @param mContext the m context
     * @param urlImage the url image
     * @return the file
     */
    public static File createFileFromURL(Context mContext, String urlImage) {
        File file = null;
        try {
            InputStream input = (InputStream) new URL(urlImage).getContent();
            file = new File(mContext.getCacheDir(), "HelpaNgo" + System.currentTimeMillis() + ".jpg");
            OutputStream out = new FileOutputStream(file);
            byte[] buf = new byte[1024];
            int len;
            while ((len = input.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            out.close();
            input.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
//		System.out.println("created File : " + file);
        return file;
    }


}
