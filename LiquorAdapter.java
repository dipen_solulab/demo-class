package com.app.bevvi.adapter;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.bevvi.R;
import com.app.bevvi.model.responseModel.ProductListing;
import com.app.bevvi.utils.Utils;
import com.bumptech.glide.Glide;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.app.bevvi.api.ApiModule.IMAGE_BASE_URL;

/**
 * Created by Nirmal on 07-11-2017.
 */
public class LiquorAdapter extends RecyclerView.Adapter<LiquorAdapter.MyViewHolder> implements Filterable {


    private Context mContext;
    private ArrayList<ProductListing> modelArrayList;
    private ArrayList<ProductListing> filterArrayList;
    private MyClickListner myClickListner;

    String formattedDate;

    /**
     * Instantiates a new Liquor adapter.
     *
     * @param context      the context
     * @param arrayList    the array list
     * @param clickListner the click listner
     */
    public LiquorAdapter(Context context, ArrayList<ProductListing> arrayList, MyClickListner clickListner) {

        this.mContext = context;

        this.modelArrayList = arrayList;
        this.filterArrayList = arrayList;
        this.myClickListner = clickListner;

        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        formattedDate = df.format(c);


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.liquor_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        ProductListing shopListModel = filterArrayList.get(position);

        if (shopListModel.getProduct() != null) {
            holder.liquorTvName.setText(shopListModel.getProduct().getName());
            holder.liquorTvType.setText(shopListModel.getProduct().getCategory());
            if (shopListModel.getRemQty() <= 0)
                holder.itemView.setAlpha(.4f);
            else
                holder.itemView.setAlpha(1f);

            holder.liquorTvOfferDay.setText("Offer expires in " + Utils.getDaysDiffrance("yyyy-MM-dd", formattedDate, shopListModel.getEndsAt()) + " days");
            if (shopListModel.getOriginalPrice().equals(shopListModel.getSalePrice())) {
                holder.liquorTvNewPrice.setVisibility(View.INVISIBLE);
                holder.liquorTvOldPrice.setBackground(null);
            } else {
                holder.liquorTvNewPrice.setVisibility(View.VISIBLE);
                holder.liquorTvOldPrice.setBackgroundResource(R.drawable.bg_strike);
            }

            holder.liquorTvOldPrice.setText("$" + String.format("%.2f", Float.valueOf(shopListModel.getOriginalPrice())));
            holder.liquorTvNewPrice.setText("$" + String.format("%.2f", Float.valueOf(shopListModel.getSalePrice())));
            if (shopListModel.getProduct().getRating() != null) {
                holder.liquorRating.setVisibility(View.VISIBLE);
                holder.liquorRating.setRating(Float.valueOf(shopListModel.getProduct().getRating()));
            } else
                holder.liquorRating.setVisibility(View.INVISIBLE);
            holder.cl_main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    myClickListner.onItemClick(v, filterArrayList.get(position));
                }
            });

            if (shopListModel.getProduct() != null && shopListModel.getProduct().getProductPhoto() != null && shopListModel.getProduct().getProductPhoto().getFile() != null)
                Glide.with(mContext).load(IMAGE_BASE_URL + shopListModel.getProduct().getProductPhoto().getFile().getFilename()).placeholder(R.drawable.image_loading).error(R.drawable.no_image).dontAnimate().into(holder.liquorImg);
            else
                Glide.with(mContext).load(R.drawable.ic_bottel).placeholder(R.drawable.image_loading).error(R.drawable.no_image).dontAnimate().into(holder.liquorImg);
        }
    }

    @Override
    public int getItemCount() {
        return filterArrayList.size();
    }

    /**
     * Gets array list.
     *
     * @return the array list
     */
    public ArrayList<ProductListing> getArrayList() {
        return filterArrayList;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                System.out.println("LiquorAdapter.performFiltering : " + charString);
                if (charString.isEmpty()) {
                    filterArrayList = modelArrayList;
                } else {
                    ArrayList<ProductListing> filteredList = new ArrayList<>();
                    for (ProductListing row : modelArrayList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getProduct().getCategory().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    filterArrayList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filterArrayList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filterArrayList = (ArrayList<ProductListing>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    /**
     * The type My view holder.
     */
    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.liquor_img)
        ImageView liquorImg;

        @BindView(R.id.liquor_tvName)
        TextView liquorTvName;

        @BindView(R.id.liquor_tvType)
        TextView liquorTvType;

        @BindView(R.id.liquor_rating)
        AppCompatRatingBar liquorRating;

        @BindView(R.id.liquor_tvOfferDay)
        TextView liquorTvOfferDay;

        @BindView(R.id.liquor_tvOldPrice)
        TextView liquorTvOldPrice;

        @BindView(R.id.liquor_tvNewPrice)
        TextView liquorTvNewPrice;

        @BindView(R.id.cl_main)
        ConstraintLayout cl_main;


        /**
         * Instantiates a new My view holder.
         *
         * @param view the view
         */
        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }
    }

    /**
     * The interface My click listner.
     */
    public interface MyClickListner {

        /**
         * On item click.
         *
         * @param view     the view
         * @param position the position
         */
        void onItemClick(View view, ProductListing position);
    }

}
