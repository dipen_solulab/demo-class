package com.app.bevvi.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.app.bevvi.R;
import com.app.bevvi.api.ApiModule;
import com.app.bevvi.api.RetrofitCallback;
import com.app.bevvi.dialog.ChangePasswordDialog;
import com.app.bevvi.dialog.LocationDialog;
import com.app.bevvi.model.responseModel.GetUserProfile;
import com.app.bevvi.model.responseModel.LoginResponse;
import com.app.bevvi.model.responseModel.PasswordError;
import com.app.bevvi.model.responseModel.SendLocationResponse;
import com.app.bevvi.model.responseModel.UserProfileUpdate;
import com.app.bevvi.services.ExampleEphemeralKeyProvider;
import com.app.bevvi.utils.Const;
import com.app.bevvi.utils.PreferenceManager;
import com.app.bevvi.utils.Utils;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.stripe.android.CustomerSession;
import com.stripe.android.model.Customer;
import com.stripe.android.model.CustomerSource;
import com.stripe.android.model.Source;
import com.stripe.android.model.SourceCardData;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SettingsActivity extends BaseActivity implements LocationDialog.MyClickListner {

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.textView35)
    TextView textView35;
    @BindView(R.id.tvLocation)
    TextView tvLocation;
    @BindView(R.id.view4)
    View view4;
    @BindView(R.id.tvCardNumber)
    TextView tvCardNumber;
    @BindView(R.id.tvPayment)
    TextView tvPayment;
    @BindView(R.id.view9)
    View view9;
    @BindView(R.id.textView39)
    TextView textView39;
    @BindView(R.id.swNotification)
    SwitchCompat swNotification;
    @BindView(R.id.tvEmail)
    TextView tvEmail;
    @BindView(R.id.edtNumber)
    EditText edtNumber;
    @BindView(R.id.reg_rediusTitle)
    TextView regRediusTitle;
    @BindView(R.id.tvPrivacy)
    TextView tvPrivacy;
    @BindView(R.id.tvFAQ)
    TextView tvFAQ;
    @BindView(R.id.tvLogOut)
    TextView tvLogOut;
    @BindView(R.id.frmWalking)
    FrameLayout frmWalking;
    @BindView(R.id.tvDistance)
    TextView tvDistance;
    @BindView(R.id.frmDriving)
    FrameLayout frmDriving;
    @BindView(R.id.tvWalking)
    TextView tvWalking;
    @BindView(R.id.tvDriving)
    TextView tvDriving;
    @BindView(R.id.ivWalk)
    ImageView ivWalk;
    @BindView(R.id.ivDriving)
    ImageView ivDriving;
    @BindView(R.id.btnDone)
    AppCompatButton btnDone;
    @BindView(R.id.cl_error)
    ConstraintLayout cl_error;


    PreferenceManager preferenceManager;

    StringBuffer sb = new StringBuffer();

    boolean isNotification = true;
    String strNumber = "";
    String selectedCustomer = "";
    int transport;
    int ADD_CARD_REQUEST_CODE = 1002;
    int CARD_LIST_REQUEST_CODE = 1003;
    double radius;

    SeekBar rangeSeekbar;
    Customer mCustomer;
    LocationDialog locationDialog;
    List<CustomerSource> customerSources = new ArrayList<>();

    @Override
    protected void onStart() {
        super.onStart();

        try {
            if (!EventBus.getDefault().isRegistered(this))
                EventBus.getDefault().register(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
//        noInternetDialog.onDestroy();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);
        preferenceManager = new PreferenceManager(this);
        tvEmail.setText(preferenceManager.getAccountDetail(PreferenceManager.ACCOUNT_DETAIL).getEmail());
        tvLocation.setText(preferenceManager.getStringPreference(PreferenceManager.PREF_ADDRESS));
        rangeSeekbar = findViewById(R.id.rangeSeekbar);

        rangeSeekbar.getProgressDrawable().setColorFilter(ContextCompat.getColor(mContext, R.color.colorLogin), PorterDuff.Mode.SRC_IN);
//        rangeSeekbar.getThumb().setColorFilter(ContextCompat.getColor(mContext, R.color.colorLogin), PorterDuff.Mode.SRC_IN);

        setToolbar();
        getUserProfileDetail();

        rangeSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                // TODO Auto-generated method stub
                if (progress >= 2) {
                    sb.delete(0, sb.length());

                    sb.append("0-").append(getConvertedValue((float) progress)).append(" mile radius");
                    tvDistance.setText(sb.toString());
                    radius = getConvertedValue((float) progress);
                }
//                Toast.makeText(seekBar.getContext(), "Value: " + getConvertedValue((float) progress), Toast.LENGTH_SHORT).show();
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }
        });

    }

    public float getConvertedValue(Float intVal) {
        float floatVal = (float) 0.0;
        floatVal = .5f * intVal;
        return floatVal;
    }

    public void initCustomerSession() {
        CustomerSession.initCustomerSession(
                new ExampleEphemeralKeyProvider(
                        new ExampleEphemeralKeyProvider.ProgressListener() {
                            @Override
                            public void onStringResponse(String string) {
                                if (string.startsWith("Error: ")) {
                                    System.out.println("Error = [" + string + "]");
                                } else {
                                    System.out.println("string = [" + string + "]");

                                }
                            }
                        }, preferenceManager.getAccountDetail(PreferenceManager.ACCOUNT_DETAIL).getPaymentIds().get(0).getToken()));
    }

    public void retrieveCustomer() {
        CustomerSession.getInstance().retrieveCurrentCustomer(
                new CustomerSession.CustomerRetrievalListener() {
                    @Override
                    public void onCustomerRetrieved(@NonNull Customer customer) {
                        Utils.progressDialogDismiss();
                        mCustomer = customer;
                        System.out.println("customer = [" + customer + "]");
                        customerSources = new ArrayList<>();
                        customerSources.addAll(customer.getSources());
                        if (customerSources.size() > 0) {

                            tvCardNumber.setVisibility(View.VISIBLE);
                            for (int i = 0; i < customerSources.size(); i++) {

                                Source source = customerSources.get(i).asSource();
                                SourceCardData sourceCardData = (SourceCardData) source.getSourceTypeModel();
                                if (selectedCustomer.equalsIgnoreCase("")) {
                                    if (source.getId().equalsIgnoreCase(customer.getDefaultSource())) {

                                        tvPayment.setText(sourceCardData.getBrand());
                                        tvCardNumber.setText(sourceCardData.getLast4());
                                        @DrawableRes int iconResourceId = Const.TEMPLATE_RESOURCE_MAP.get(sourceCardData.getBrand());
                                        tvCardNumber.setCompoundDrawablesWithIntrinsicBounds(iconResourceId, 0, 0, 0);
                                        tvCardNumber.setCompoundDrawablePadding(10);
                                    }
                                } else {
                                    if (source.getId().equalsIgnoreCase(selectedCustomer)) {
                                        tvPayment.setText(sourceCardData.getBrand());
                                        tvCardNumber.setText(sourceCardData.getLast4());
                                        @DrawableRes int iconResourceId = Const.TEMPLATE_RESOURCE_MAP.get(sourceCardData.getBrand());
                                        tvCardNumber.setCompoundDrawablesWithIntrinsicBounds(iconResourceId, 0, 0, 0);
                                        tvCardNumber.setCompoundDrawablePadding(10);
                                    }
                                }
                            }
                        } else {
                            tvCardNumber.setText("Add a card");
                        }

                    }

                    @Override
                    public void onError(int errorCode, @Nullable String errorMessage) {
                        Utils.progressDialogDismiss();
                        Utils.makeSnakebare(toolbar, errorMessage);
                        System.out.println("errorCode = [" + errorCode + "], errorMessage = [" + errorMessage + "]");
                    }
                });

    }

    private void getUserProfileDetail() {
        Utils.progressDialog(mContext);
        LoginResponse loginResponse = preferenceManager.getLoginData();
        JsonObject object = new JsonObject();
        JsonObject where = new JsonObject();
        where.addProperty("accountId", loginResponse.getUserId());
        object.add("where", where);
        JsonArray include = new JsonArray();
        include.add("account");
        object.add("include", include);


        Call<ArrayList<GetUserProfile>> call = ApiModule.apiService().getUserProfileDetail(object.toString(), loginResponse.getAccessToken());
        call.enqueue(new RetrofitCallback<ArrayList<GetUserProfile>>(mContext, toolbar, new Callback<ArrayList<GetUserProfile>>() {

            @Override
            public void onResponse(Call<ArrayList<GetUserProfile>> call, Response<ArrayList<GetUserProfile>> response) {


                initCustomerSession();
                retrieveCustomer();
                if (response.isSuccessful()) {
                    if (!response.body().isEmpty()) {
                        if (response.body().get(0).getAccount().getPhoneNumber() != null)
                            strNumber = response.body().get(0).getAccount().getPhoneNumber();
                        edtNumber.setText(strNumber);
                        if (response.body().get(0).getNotifications())
                            swNotification.setChecked(true);
                        else
                            swNotification.setChecked(false);
                        setTrasnport(response.body().get(0).getTransport());
//                        rangeSeekbar.setMinStartValue(3);
//                        rangeSeekbar.setMaxStartValue(5);
//                        rangeSeekbar.apply();
                        radius = response.body().get(0).getRadius();
                        if (response.body().get(0).getRadius() != null)
                            preferenceManager.setStringPreference(PreferenceManager.RADIUS, String.valueOf(radius));
                        if (response.body().get(0).getTransport() != null)
                            preferenceManager.setIntPreference(PreferenceManager.TRANSPORT, response.body().get(0).getTransport());
//                        rangeSeekbar.setRight((int) response.body().get(0).getRadius());
                        rangeSeekbar.setProgress((int) Math.ceil(response.body().get(0).getRadius()));

                        sb.delete(0, sb.length());
                        sb.append("0-").append(response.body().get(0).getRadius()).append(" mile radius");
                        tvDistance.setText(sb.toString());
                        Gson gson = new Gson();
                        GetUserProfile accountDetail = response.body().get(0);

                        String accountData = gson.toJson(accountDetail);

                        preferenceManager.setStringPreference(PreferenceManager.PROFILE_DETAIL, accountData);

                    }
                } else if (response.raw().code() == 401) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (!isFinishing()) {

                                new AlertDialog.Builder(mContext).setTitle("Alert").setMessage(R.string.session_expire).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        PreferenceManager preferenceManager = new PreferenceManager(mContext);
                                        preferenceManager.clearPrefrences();
                                        preferenceManager.setStringPreference(PreferenceManager.FCM_TOKEN, FirebaseInstanceId.getInstance().getToken());
                                        Intent intent = new Intent(mContext, LoginActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        finish();
                                        startActivity(intent);
                                        finishAffinity();

                                    }
                                }).setCancelable(false).show();

                            }
                        }
                    });

                }

            }

            @Override
            public void onFailure(Call<ArrayList<GetUserProfile>> call, Throwable t) {
                System.out.println("SettingsActivity.onFailure : " + t.getMessage());
                Utils.progressDialogDismiss();

            }
        }));

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Utils.progressDialog(mContext);
            initCustomerSession();
            retrieveCustomer();
        }
    }

    private void updatePhoneNumber(String number) {

        LoginResponse loginResponse = preferenceManager.getLoginData();
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("phoneNumber", number);

//        Utils.progressDialog(mContext);
        Call<ResponseBody> call = ApiModule.apiService().addPhoneNumber(loginResponse.getUserId(), jsonObject, loginResponse.getAccessToken());
        call.enqueue(new RetrofitCallback<ResponseBody>(mContext, toolbar, new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                Utils.progressDialogDismiss();
                if (response.raw().code() == 401) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (!isFinishing()) {

                                new AlertDialog.Builder(mContext).setTitle("Alert").setMessage(R.string.session_expire).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        PreferenceManager preferenceManager = new PreferenceManager(mContext);
                                        preferenceManager.clearPrefrences();
                                        preferenceManager.setStringPreference(PreferenceManager.FCM_TOKEN, FirebaseInstanceId.getInstance().getToken());
                                        Intent intent = new Intent(mContext, LoginActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        finish();
                                        startActivity(intent);
                                        finishAffinity();

                                    }
                                }).setCancelable(false).show();

                            }
                        }
                    });

                } else if (response.isSuccessful()) {
                    onBackPressed();
                }
                System.out.println("updatePhoneNumber.onResponse");

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                System.out.println("updatePhoneNumber.onFailure : " + t.getMessage());
                Utils.progressDialogDismiss();

            }
        }));

    }


    void setTrasnport(int trasnport) {
        this.transport = trasnport;
        if (trasnport == 0) {
            frmWalking.setBackgroundResource(R.drawable.bg_button_login);
            Drawable icon = getResources().getDrawable(R.drawable.ic_walk);
            icon.setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_IN);
            ivWalk.setImageDrawable(icon);


            Drawable icon1 = getResources().getDrawable(R.drawable.ic_drive);
            icon1.setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
            ivDriving.setImageDrawable(icon1);
//            ivDriving.setImageResource(R.drawable.ic_drive);

            tvWalking.setTextColor(Color.WHITE);
            frmDriving.setBackground(null);
            tvDriving.setTextColor(Color.BLACK);
        } else {
            Drawable icon = getResources().getDrawable(R.drawable.ic_drive);
            icon.setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_IN);
            ivDriving.setImageDrawable(icon);


            Drawable icon1 = getResources().getDrawable(R.drawable.ic_walk);
            icon1.setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
            ivWalk.setImageDrawable(icon1);
//            ivWalk.setImageResource(R.drawable.ic_walk);

            frmDriving.setBackgroundResource(R.drawable.bg_button_login);
            tvDriving.setTextColor(Color.WHITE);
            frmWalking.setBackground(null);
            tvWalking.setTextColor(Color.BLACK);
        }
    }


    AlertDialog changePasswordDialog;

    @OnClick({R.id.ll_resetPassword, R.id.tvLocation, R.id.tvLogOut, R.id.btnDone, R.id.tvWalking, R.id.tvDriving, R.id.tvCardNumber, R.id.tvPayment, R.id.tvPrivacy, R.id.tvTerms, R.id.tvFAQ})
    public void onViewClicked(View view) {
        Intent intent = new Intent(mContext, WebViewActivity.class);
        switch (view.getId()) {
            case R.id.ll_resetPassword:

                changePasswordDialog = new ChangePasswordDialog(mContext, new ChangePasswordDialog.DialogClickListner() {
                    @Override
                    public void onClickOK(String password) {

                        changePasswordDialog.dismiss();
                        LoginResponse loginResponse = preferenceManager.getLoginData();

                        changePassword(loginResponse.getUserId(), loginResponse.getAccessToken(), password);

                    }
                });
                changePasswordDialog.show();
                changePasswordDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);

                break;
            case R.id.tvLocation:
                locationDialog = new LocationDialog(this, this, null, this);
                break;
            case R.id.tvLogOut:
                new AlertDialog.Builder(this).setCancelable(false).setMessage("Are you sure want to Logout?").setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        deleteAllAccessToken();

                    }
                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
                break;
            case R.id.btnDone:
                Utils.progressDialog(mContext);
                updateUserProfile(false);
                break;
            case R.id.tvWalking:
                transport = 0;
                setTrasnport(transport);
                break;
            case R.id.tvDriving:
                transport = 1;
                setTrasnport(transport);
                break;
            case R.id.tvCardNumber:
                callPaymentOptions();
                break;
            case R.id.tvPayment:
                callPaymentOptions();
                break;
            case R.id.tvPrivacy:
                intent.putExtra("Privacy", "Privacy");
                startActivity(intent);
                break;
            case R.id.tvTerms:
                intent.putExtra("Terms", "Terms");
                startActivity(intent);
                break;
            case R.id.tvFAQ:
                intent.putExtra("FAQ", "FAQ");
                startActivity(intent);
                break;
        }
    }

    private void changePassword(String id, String token, String password) {

        Utils.progressDialog(mContext);


        Call<PasswordError> call = ApiModule.apiService().changePassword(id, password, token);
        call.enqueue(new RetrofitCallback<PasswordError>(mContext, toolbar, new Callback<PasswordError>() {

            @Override
            public void onResponse(Call<PasswordError> call, Response<PasswordError> response) {
                Utils.progressDialogDismiss();
                if (response.isSuccessful()) {
                    System.out.println("changePassword.isSuccessful");
                    if (response.body() != null && response.body().getError() != null) {

                        Utils.makeDialogWithOk(mContext,response.body().getError().getMessage());

                    } else {
                        showToast("Password Change successfully", mContext);
                    }
                } else {
                    System.out.println("changePassword.fail");
                    Gson gson = new GsonBuilder().create();
                    PasswordError errorModel = new PasswordError();
                    try {
                        errorModel = gson.fromJson(response.errorBody().string(), PasswordError.class);

                        Utils.makeDialogWithOk(mContext, errorModel.getError().getMessage());

//                        Toast.makeText(mContext, errorModel.getError().getDetails().getMessages().getEmail().get(0), Toast.LENGTH_LONG).show();
                    } catch (IOException e) {
                        // handle failure to read error
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<PasswordError> call, Throwable t) {
                System.out.println("setOrderViewed.onFailure : " + t.getMessage());
                Utils.progressDialogDismiss();
            }
        }));

    }

    private void callPaymentOptions() {
        if (customerSources.size() == 0) {
            Intent intent = new Intent(SettingsActivity.this, AddCardActivity.class);
            startActivityForResult(intent, ADD_CARD_REQUEST_CODE);
        } else {
            Intent intent = new Intent(SettingsActivity.this, CardListActivity.class);
            startActivityForResult(intent, CARD_LIST_REQUEST_CODE);
        }
    }

    private void updateUserProfile(final boolean isBackPress) {
//         String number = edtNumber.getText().toString().trim();

        LoginResponse loginResponse = preferenceManager.getLoginData();
        JsonObject where = new JsonObject();
        where.addProperty("accountId", loginResponse.getUserId());


        JsonObject object = new JsonObject();
        object.addProperty("accountId", loginResponse.getUserId());
        object.addProperty("radius", radius);
        object.addProperty("transport", transport);

        isNotification = swNotification.isChecked();
        object.addProperty("notifications", isNotification);


        Call<UserProfileUpdate> call = ApiModule.apiService().updateUserProfile(where.toString(), object, loginResponse.getAccessToken());
        call.enqueue(new RetrofitCallback<UserProfileUpdate>(mContext, toolbar, new Callback<UserProfileUpdate>() {

            @Override
            public void onResponse(Call<UserProfileUpdate> call, Response<UserProfileUpdate> response) {

                Utils.progressDialogDismiss();
                if (response.isSuccessful()) {

                    preferenceManager.setStringPreference(PreferenceManager.RADIUS, String.valueOf(response.body().getRadius()));
                    preferenceManager.setIntPreference(PreferenceManager.TRANSPORT, response.body().getTransport());
                    String number = edtNumber.getText().toString().trim();
                    if (!TextUtils.isEmpty(number)) {
                        if (!strNumber.equalsIgnoreCase(number)) {
                            updatePhoneNumber(number);
                        } else
                            onBackPressed();

                    } else if (!isBackPress)
                        onBackPressed();

                } else if (response.raw().code() == 401) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (!isFinishing()) {

                                new AlertDialog.Builder(mContext).setTitle("Alert").setMessage(R.string.session_expire).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        PreferenceManager preferenceManager = new PreferenceManager(mContext);
                                        preferenceManager.clearPrefrences();
                                        preferenceManager.setStringPreference(PreferenceManager.FCM_TOKEN, FirebaseInstanceId.getInstance().getToken());
                                        Intent intent = new Intent(mContext, LoginActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        finish();
                                        startActivity(intent);
                                        finishAffinity();

                                    }
                                }).setCancelable(false).show();

                            }
                        }
                    });


                }

            }

            @Override
            public void onFailure(Call<UserProfileUpdate> call, Throwable t) {
                System.out.println("SettingsActivity.onFailure : " + t.getMessage());
                Utils.progressDialogDismiss();

            }
        }));

    }

    private void setToolbar() {

        toolbarTitle.setText("Settings");
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                updateUserProfile(true);
                onBackPressed();
            }
        });


    }

    private void deleteAllAccessToken() {
        LoginResponse loginResponse = preferenceManager.getLoginData();
        Utils.progressDialog(mContext);
        Call<ResponseBody> call = ApiModule.apiService().deleteAllAccessToken(loginResponse.getUserId(), loginResponse.getAccessToken());
        call.enqueue(new RetrofitCallback<ResponseBody>(mContext, toolbar, new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {


                System.out.println("deleteAllAccessToken.onResponse : deleteAllAccessToken ");
//                hideAnimation();
                if (response.isSuccessful())
                    callLogout();
                else {
                    Utils.progressDialogDismiss();
                    showToast("Something went wrong", mContext);
                }

//

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                System.out.println("deleteAllAccessToken.onFailure : " + t.getMessage());
                Utils.progressDialogDismiss();
            }
        }));
    }

    void callLogout() {
//        Utils.progressDialog(this);
        LoginResponse loginResponse = preferenceManager.getLoginData();

        Call<ResponseBody> call = ApiModule.apiService().doLogout(loginResponse.getAccessToken());
        call.enqueue(new RetrofitCallback<ResponseBody>(mContext, toolbar, new Callback<ResponseBody>() {


            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                Utils.progressDialogDismiss();
                System.out.println("SettingsActivity.onResponse");
                preferenceManager.clearPrefrences();
                preferenceManager.setStringPreference(PreferenceManager.FCM_TOKEN, FirebaseInstanceId.getInstance().getToken());
                Intent intent = new Intent(mContext, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                finish();
                startActivity(intent);
                finishAffinity();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                System.out.println("callLogout.onFailure : " + t.getMessage());
                Utils.progressDialogDismiss();
            }
        }));
    }


    @Override
    public void onGetLocation(boolean isForSearch) {
        locationDialog.dismissDialog();
        if (isForSearch) {
            sendSearchLocation(Double.valueOf(preferenceManager.getStringPreference(PreferenceManager.PREF_LAT)), Double.valueOf(preferenceManager.getStringPreference(PreferenceManager.PREF_LONG)), preferenceManager.getStringPreference(PreferenceManager.PREF_ADDRESS));
        } else {
            tvLocation.setText(preferenceManager.getStringPreference(PreferenceManager.PREF_ADDRESS));
            locationDialog.dismissDialog();
        }
    }

    private void sendSearchLocation(final double lat, final double lon, final String address) {

        /*{
            "geoLocation" : {
                    "type" : "Point",
                    "coordinates" : [
                    -118.4109089,
                    33.884736100000005
                    ]
                }
        }*/

        JsonObject jsonObject = new JsonObject();
        JsonObject geoLocation = new JsonObject();

        JsonArray elements = new JsonArray();
        elements.add(lon);
        elements.add(lat);

        geoLocation.add("coordinates", elements);
        geoLocation.addProperty("type", "Point");

        jsonObject.add("geoLocation", geoLocation);

        Utils.progressDialog(mContext);
        Call<SendLocationResponse> call = ApiModule.apiService().sendSearchLocation(jsonObject);
        call.enqueue(new RetrofitCallback<SendLocationResponse>(mContext, toolbar, new Callback<SendLocationResponse>() {

            @Override
            public void onResponse(Call<SendLocationResponse> call, Response<SendLocationResponse> response) {
//                if (Utils.isFragmentVisible(HomeFragment.this))
                {
                    if (response.isSuccessful()) {
                        LoginResponse loginResponse = preferenceManager.getLoginData();
                        if (loginResponse != null)
                            addLocationToSearchHistory(loginResponse.getUserId(), lat, lon, response.body().getId(), address);
                        else
                            Utils.progressDialogDismiss();
                    }
                    tvLocation.setText(preferenceManager.getStringPreference(PreferenceManager.PREF_ADDRESS));
                }
            }

            @Override
            public void onFailure(Call<SendLocationResponse> call, Throwable t) {
                System.out.println("HomeFragment.onFailure : " + t.getMessage());
                Utils.progressDialogDismiss();

            }
        }));


    }

    private void addLocationToSearchHistory(String userID, double lat, double lon, String locationId, String address) {
        /*{
            "accountId" : "5a5dd89ace6f114067cf8889",
                "geoLocation" : {
                   "type" : "Point",
                    "coordinates" : [
                     -118.4109089,
                    33.884736100000005
                  ]
            },
            "locationId" : "5a5ee4214dc5534f69f732f7",
                "address" : "Manhattan Beach, CA, USA"
        }*/

        JsonObject jsonObject = new JsonObject();
        JsonObject geoLocation = new JsonObject();

        JsonArray elements = new JsonArray();
        elements.add(lon);
        elements.add(lat);

        geoLocation.add("coordinates", elements);
        geoLocation.addProperty("type", "Point");

        jsonObject.addProperty("accountId", userID);
        jsonObject.add("geoLocation", geoLocation);
        jsonObject.addProperty("locationId", locationId);
        jsonObject.addProperty("address", address);


        Call<ResponseBody> call = ApiModule.apiService().addLocationToSearchHistory(jsonObject);
        call.enqueue(new RetrofitCallback<ResponseBody>(mContext, toolbar, new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                Utils.progressDialogDismiss();

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                System.out.println("HomeFragment.onFailure : " + t.getMessage());
                Utils.progressDialogDismiss();

            }
        }));


    }

    @OnClick(R.id.btnRetry)
    public void onRetryClick() {
        cl_error.setVisibility(View.GONE);
        Intent intent = getIntent();
        startActivity(intent);
        finish();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getData(String data) {
        if (data.equalsIgnoreCase("ConnectException")) {
            cl_error.setVisibility(View.VISIBLE);
        }
    }
}
