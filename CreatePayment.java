package com.app.bevvi.services;

import android.support.annotation.NonNull;

import com.app.bevvi.api.ApiService;
import com.app.bevvi.api.RetrofitFactory;
import com.stripe.android.EphemeralKeyProvider;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Retrofit;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * An implementation of {@link EphemeralKeyProvider} that can be used to generate
 * ephemeral keys on the backend.
 */
public class CreatePayment {

    private @NonNull
    CompositeSubscription mCompositeSubscription;
    private @NonNull
    ApiService mStripeService;
    private @NonNull
    ProgressListener mProgressListener;
    /**
     * The Api param map.
     */
    Map<String, Object> apiParamMap = new HashMap<>();

    /**
     * Instantiates a new Create payment.
     *
     * @param progressListener the progress listener
     * @param apiParam         the api param
     */
    public CreatePayment(@NonNull ProgressListener progressListener, Map<String, Object> apiParam) {
        Retrofit retrofit = RetrofitFactory.getInstance();
        mStripeService = retrofit.create(ApiService.class);
        mCompositeSubscription = new CompositeSubscription();
        mProgressListener = progressListener;
        apiParamMap.clear();
        apiParamMap.putAll(apiParam);

        mCompositeSubscription.add(
                mStripeService.createCharge(apiParamMap)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Action1<ResponseBody>() {
                            @Override
                            public void call(ResponseBody response) {
                                String rawKey = "";//response.string();

                                JSONArray jsonArray = null;
                                try {
                                    jsonArray = new JSONArray(response.string());
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                mProgressListener.onStringResponse(jsonArray);
                            }
                        }, new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                mProgressListener.onErrorResponse(throwable.getMessage());
                            }
                        }));
    }

    /**
     * The interface Progress listener.
     */
    public interface ProgressListener {
        /**
         * On string response.
         *
         * @param string the string
         */
        void onStringResponse(JSONArray string);

        /**
         * On error response.
         *
         * @param string the string
         */
        void onErrorResponse(String string);
    }
}
